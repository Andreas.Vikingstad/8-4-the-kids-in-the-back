# Medlemmer
* Erik, Andreas, Nicolai, Elisabeth og Snorre

## Gruppenavn 
* The Kids in the Back

## Gruppenummer
* Gruppe 4

## Prosjektnavn
* DungeonRun

## controllers
* 'Høyre piltast' Beveger seg til høyre
* 'Venstre piltast' Beveger seg til venstre
* 'nedover piltast' går gjennom 'phasable' blokker
* 'Mellomrom* hoppeknapp
* 'P' pause/play
* '1,2,3,4,5' for å velge "skin" i pausemenyen og fortsette spillet

## Hvordan kjøres spillet
* Kompileres med 'mvn package'
* Kjøres med 8-4-THE-KIDS-IN-THE-BACK-1.0-SNAPSHOT.jar

## Game Description
* Spillfigur (som er deg) kan styres med piltaster og space-bar. 
* Todimensjonal verden:
   * Plattform – horisontal flate spilleren kan stå eller gå på (inkludert «bakken»)
   * Vegg – vertikal flate som spilleren ikke kan gå gjennom
   * Spilleren beveger seg oppover ved å hoppe, og nedover ved å falle
* Fiender som beveger seg og er skadelige ved berøring
* Spilleren kan samle ekstra hjerter for å øke antall liv
* Spilleren kan samle inn cosmetics for å customize karakteren
* Utfordringen i spillet:
	-jobbe seg nedover i forskjellige rom
	-overkomme monster
	-nå datamaskinen
* Spiller må finne nøkkel for å låse opp døra.

## Game Objective
*“Å nei!! du tok eksamen i INF112 og resultatet er for dårlig. DU må nå ta deg ut på en EPIC QUEST i søk om å endre INF112 karakteren din. Du må bevege deg ned i kjelleren på RFB for å finne passordet til maskinen til Anya og redde Karakteren din” 
*Finn nøkkel for å låse opp dørene og beveg deg til neste level.


## Credits
* Bilder og lyder er laget av medlemmene av prosjektet

## Teknisk beskrivelse
* Prosjektet anvender model - view - controller oppsett for å strukturere koden.
* Av egne objekter brukt i modellen for spillogikk så er de strukturert i entities, levels og tiles
* Tiles er selve rutene som gjør opp banen og hvordan den ser ut
* Entitiesa er enhetene som befolker banen. Dette er alt fra spilleren, til fiendene og powerupsa.
* PlatformerLevel er der banen er representert, med alle enheter inne.
* PlatformerLevelFactory er objektfabrikken som lager PlatformerLevel fra en streng.
* Modellen inneholder metodene for å kunne bevege spilleren gjennom levelet. Disse kalles på fra kontrolleren gjennom det begrensede grensesnittet ControllablePlatformerModel
* Informasjonen til viewet sendes fra modellen gjennom det begrensede grensesnittet ViewablePlatformerModel.
* Varsler om at viewet skal repaintes eller at en lyd skal spilles sendes gjennom en eventBus.
* Et klassediagram kan finnes i doc-mappen
