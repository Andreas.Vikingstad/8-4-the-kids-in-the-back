package inf112.skeleton.eventBus;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EventBusTest {
  
  String i;

  @BeforeEach
  public void setUp() {
    i = "";
  }

  @Test
  public void eventTest() {
    EventBus eventBus = new EventBus();
    eventBus.register(this::handle);
    eventBus.post(new SoundEvent("hello"));
    assertEquals("hello", i);
  }

  private void handle(Event event) {
    i = ((SoundEvent) event).soundName();
  }
}
