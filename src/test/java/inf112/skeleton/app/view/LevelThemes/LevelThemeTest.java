package inf112.skeleton.app.view.LevelThemes;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.awt.Image;

public class LevelThemeTest {

  LevelTheme theme = new DefaultLevelTheme("default");


  @Test
  public void getSpriteTest() {
    Image sprite = theme.getSprite('#', 0);
    assertEquals(sprite.getWidth(null), 20);
    assertEquals(sprite.getHeight(null), 20);
  }

  @Test
  public void getPlayerSpriteTest() {
    for (int i = 1; i < 6; i++) {
      Image sprite = theme.getPlayerSprite(i);
      assertEquals(sprite.getWidth(null), 20);
      assertEquals(sprite.getHeight(null), 20);
    }
  }

  @Test
  public void getBackgroundSpriteTest() {
    Image sprite = theme.getBackgroundSprite();
    assertEquals(sprite.getWidth(null), 20);
    assertEquals(sprite.getHeight(null), 20);
  }

  @Test
  public void playSoundTest(){
    assertTrue(theme.playSound("locked"));
    assertFalse(theme.playSound("makeFalse"));
  }
}
