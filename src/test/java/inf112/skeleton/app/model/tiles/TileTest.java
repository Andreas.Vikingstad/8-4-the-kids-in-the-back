package inf112.skeleton.app.model.tiles;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Rectangle;

import org.junit.jupiter.api.Test;

public class TileTest {

  Rectangle box = new Rectangle(0, 0, 20, 20);
  Tile tile = new Tile(box);

  @Test
  public void isSolidTest() {
    assertFalse(tile.isSolid());
  }

  @Test
  public void getSymbolTest() {
    assertThrows(NullPointerException.class, () -> {
    tile.getChar();
    });
  }

  @Test
  public void isPhaseableTest() {
    assertFalse(tile.isPhaseable());
  }

}
