package inf112.skeleton.app.model.tiles;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Rectangle;

import org.junit.jupiter.api.Test;

public class WallTest {

  Rectangle box = new Rectangle(0, 0, 20, 20);
  Wall wall = new Wall(box);

  @Test
  public void definedUndefinedFieldsTest() {
    assertEquals(wall.getChar(), '#');
    assertTrue(wall.isSolid());
  }
}
