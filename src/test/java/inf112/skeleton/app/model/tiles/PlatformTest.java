package inf112.skeleton.app.model.tiles;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Rectangle;

import org.junit.jupiter.api.Test;

public class PlatformTest {

  Rectangle box = new Rectangle(0, 0, 20, 20);
  Platform platform = new Platform(box);

  @Test
  public void definedUndefinedFieldsTest() {
    assertEquals(platform.getChar(), '_');
    assertTrue(platform.isSolid());
    assertTrue(platform.isPhaseable());
  }
}
