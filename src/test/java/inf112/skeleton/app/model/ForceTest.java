package inf112.skeleton.app.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class ForceTest {
  
  @Test
  public void equalsTest() {
    Force force = new Force(1, 1);
    Force force2 = new Force(1, 1);

    assertFalse(force.equals(null));
    assertTrue(force.equals(force));
    assertFalse(force.equals(new Object()));
    assertTrue(force.equals(force2));
    force2 = new Force(2, 2);
    assertFalse(force.equals(force2));
    force2 = new Force(1, 2);
    assertFalse(force.equals(force2));
    force2 = new Force(2, 1);
    assertFalse(force.equals(force2));
  }

  @Test
  public void hashCodeTest() {
    Force force = new Force(1, 1);
    Force force2 = new Force(1, 1);

    assertEquals(force.hashCode(), force2.hashCode());
    force2 = new Force(2, 2);
    assertNotEquals(force.hashCode(), force2.hashCode());
  }

  @Test
  public void toStringTest() {
    Force force = new Force(1, 1);
    assertEquals("Force: xForce = 1, yForce = 1", force.toString());
  }

  @Test
  public void changeForceTest() {
    Force force = new Force(1, 1);
    force = force.changeForce(2, 2);
    assertEquals(3, force.xForce());
    assertEquals(3, force.yForce());
  }

  @Test
  public void setXForceTest() {
    Force force = new Force(1, 1);
    force = force.setXForce(2);
    assertEquals(2, force.xForce());
  }

  @Test
  public void setYForceTest() {
    Force force = new Force(1, 1);
    force = force.setYForce(2);
    assertEquals(2, force.yForce());
  }

  @Test
  public void changeXForceTest() {
    Force force = new Force(1, 1);
    force = force.changeXForce(2);
    assertEquals(3, force.xForce());
  }

  @Test
  public void changeYForceTest() {
    Force force = new Force(1, 1);
    force = force.changeYForce(2);
    assertEquals(3, force.yForce());
  }

}
