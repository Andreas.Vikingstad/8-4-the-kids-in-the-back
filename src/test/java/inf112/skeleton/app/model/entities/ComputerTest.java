package inf112.skeleton.app.model.entities;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class ComputerTest {
  
  @Test
  public void definedUndefinedFieldsTest() {
    Computer entity = new Computer(null);
    assertTrue(entity.getChar() == 'C');
  }

}
