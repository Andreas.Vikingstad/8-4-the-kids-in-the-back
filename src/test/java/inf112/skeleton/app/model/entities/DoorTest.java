package inf112.skeleton.app.model.entities;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class DoorTest {
  
  @Test
  public void definedUndefinedFieldsTest() {
    Door entity = new Door(null, false);
    assertTrue(entity.getChar() == 'D');
    assertTrue(entity.isLocked() == false);
    entity.setLocked(true);
    assertTrue(entity.isLocked() == true);
    
  }
}
