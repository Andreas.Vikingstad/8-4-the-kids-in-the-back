package inf112.skeleton.app.model.entities;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class HealthBoostTest {
  
  @Test
  public void definedUndefinedFieldsTest() {
    HealthBoost entity = new HealthBoost(null);
    assertTrue(entity.getChar() == 'H');
    assertTrue(entity.getDamage() == -1);
  }

}
