package inf112.skeleton.app.model.entities;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.awt.Rectangle;

import inf112.skeleton.app.model.Force;

public class EnemyTest {

  @Test
  public void definedUndefinedFieldsTest() {
    Rectangle hitbox = new Rectangle(2, 2, 2, 2);
    Force force = new Force(1, 1);
    Enemy entity = new Enemy(hitbox, force);

    assertTrue(entity.getChar() == 'E');
    assertTrue(entity.getHealth() == 1);
    assertTrue(entity.getDamage() == 1);
  }
}
