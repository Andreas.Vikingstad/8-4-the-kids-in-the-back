package inf112.skeleton.app.model.entities;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.awt.Rectangle;

import org.junit.jupiter.api.Test;

import inf112.skeleton.app.model.Force;

public class MovableEntityTest {
  
  @Test
  public void getForceTest() {
    Rectangle hitbox = new Rectangle(2, 2, 2, 2);
    Force force = new Force(1, 1);
    MovableEntity entity = new MovableEntity(hitbox, force);

    assertTrue(entity.getForce().equals(force));
  }

  @Test
  public void setForceTest() {
    Rectangle hitbox = new Rectangle(2, 2, 2, 2);
    Force force = new Force(1, 1);
    MovableEntity entity = new MovableEntity(hitbox, force);

    Force newForce = new Force(2, 2);
    entity.setForce(newForce);

    assertTrue(entity.getForce().equals(newForce));
  }
  
  @Test
  public void shiftedByTest() {
    Force force = new Force(0, 0);
    MovableEntity entity = new MovableEntity(new Rectangle(0, 0, 1, 1), force);
    Rectangle moved = entity.shiftedBy(0, 1);
    assertTrue(moved.x == 0);
    assertTrue(moved.y == 1);

    moved = entity.shiftedBy(1, 0);
    assertTrue(moved.x == 1);
    assertTrue(moved.y == 0);

    entity = new MovableEntity(moved, force);
    moved = entity.shiftedBy(0, 1);
    assertTrue(moved.x == 1);
    assertTrue(moved.y == 1);
  }

  @Test
  public void moveTest() {
    Rectangle hitbox = new Rectangle(0, 0, 1, 1);
    Force force = new Force(0, 0);
    MovableEntity entity = new MovableEntity(hitbox, force);
    entity.move(0, 1);
    assertTrue(entity.getHitbox().x == 0);
    assertTrue(entity.getHitbox().y == 1);

    entity.move(1, 0);
    assertTrue(entity.getHitbox().x == 1);
    assertTrue(entity.getHitbox().y == 1);
  }
}
