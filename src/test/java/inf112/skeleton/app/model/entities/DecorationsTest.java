package inf112.skeleton.app.model.entities;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class DecorationsTest {
  
  @Test
  public void definedUndefinedFieldsTest() {
    Decorations entity = new Decorations(null, "blue_chair");
    assertTrue(entity.getChar() == 'F');
    assertTrue(entity.getType().equals("blue_chair"));
    assertFalse(entity.getType().equals("red_chair"));
  }

}
