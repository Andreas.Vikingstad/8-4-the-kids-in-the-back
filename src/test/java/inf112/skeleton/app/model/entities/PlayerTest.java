package inf112.skeleton.app.model.entities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.awt.Rectangle;

import org.junit.jupiter.api.Test;

import inf112.skeleton.app.model.Force;

public class PlayerTest {
  
  @Test
  public void definedUndefinedFieldsTest() {
    Rectangle hitbox = new Rectangle(2, 2, 2, 2);
    Force force = new Force(1, 1);
    Player entity = new Player(hitbox, force);

    assertEquals(entity.getChar(), 'P');
    assertEquals(entity.getHealth(), 3);
    assertEquals(entity.getDamage(), 1);
  }

  @Test
  public void getMovingTest() {
    Rectangle hitbox = new Rectangle(2, 2, 2, 2);
    Force force = new Force(1, 1);
    Player entity = new Player(hitbox, force);

    assertTrue(entity.getMoving() == 0);
  }

  @Test
  public void changeMovingTest() {
    Rectangle hitbox = new Rectangle(2, 2, 2, 2);
    Force force = new Force(1, 1);
    Player entity = new Player(hitbox, force);

    entity.changeMoving(1);
    assertTrue(entity.getMoving() == 1);

    entity.changeMoving(-1);
    assertTrue(entity.getMoving() == 0);

    entity.changeMoving(-1);
    assertTrue(entity.getMoving() == -1);
  }
  
}
