package inf112.skeleton.app.model.entities;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class HealthTest {

  //Tests that the getHealth method returns the correct health
  @Test
  public void getHealthTest() {
    Player player = new Player(null, null);
    Player.lives = 1;
    assertEquals(player.getHealth(), 1);
    Player.lives = 2;
    assertEquals(player.getHealth(), 2);
    Player.lives = 3;
    assertEquals(player.getHealth(), 3);
  }

  //Tests that player loses 1 life when attacked by enemy
  @Test
  public void loseLifeTest() {
    Player player = new Player(null, null);
    Enemy enemy = new Enemy(null, null);
    player.takeDamage(enemy.getDamage());
    assertEquals(player.getHealth(), 2);
    player.takeDamage(enemy.getDamage());
    assertEquals(player.getHealth(), 1);
  }

  //Tests that player gains 1 life when getting a health boost
  @Test
  public void gainLifeTest() {
    Player player = new Player(null, null);
    player.takeDamage(1);
    assertEquals(player.getHealth(), 2);
    HealthBoost healthBoost = new HealthBoost(null);
    player.takeDamage(healthBoost.getDamage());
    assertEquals(player.getHealth(), 3);
  }

  //maxHealth should always return 3
  @Test
  public void maxHealthTest() {
    Player player = new Player(null, null);
    assertEquals(player.maxHealth(), 3);
  }

}
