package inf112.skeleton.app.model.entities;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Rectangle;

import org.junit.jupiter.api.Test;

import inf112.skeleton.app.model.Force;

public class ControllableEntityTest {
  
  @Test
  public void definedUndefinedFieldsTest() {
    Rectangle hitbox = new Rectangle(2, 2, 2, 2);
    Force force = new Force(1, 1);
    ControllableEntity entity = new ControllableEntity(hitbox, force);
    assertTrue(entity.getMoving() == 0);
    assertTrue(entity.isOnWall() == 0);
  }

  @Test
  public void moveLeftTest() {
    Rectangle hitbox = new Rectangle(2, 2, 2, 2);
    Force force = new Force(1, 1);
    ControllableEntity entity = new ControllableEntity(hitbox, force);
    assertTrue(entity.moveLeft(true));
    assertTrue(entity.getMoving() == -1);
    assertFalse(entity.moveLeft(false));
    assertTrue(entity.getMoving() == 0);
  }

  @Test
  public void moveRightTest() {
    Rectangle hitbox = new Rectangle(2, 2, 2, 2);
    Force force = new Force(1, 1);
    ControllableEntity entity = new ControllableEntity(hitbox, force);
    assertTrue(entity.moveRight(true));
    assertTrue(entity.getMoving() == 1);
    assertFalse(entity.moveRight(false));
    assertTrue(entity.getMoving() == 0);
  }

  @Test
  public void jumpTest() {
    Rectangle hitbox = new Rectangle(2, 2, 2, 2);
    Force force = new Force(0, 0);
    ControllableEntity entity = new ControllableEntity(hitbox, force);
    assertFalse(entity.jump());
    entity.setOnGround(true);
    assertTrue(entity.jump());
    assertTrue(entity.getForce().yForce() == -10);
    entity = new ControllableEntity(hitbox, force);
    entity.setOnWall(-1);
    assertTrue(entity.jump());
    assertTrue(entity.getForce().xForce() == 7);
    assertTrue(entity.getForce().yForce() == -10);
    entity = new ControllableEntity(hitbox, force);
    entity.setOnWall(1);
    assertTrue(entity.jump());
    assertTrue(entity.getForce().xForce() == -7);
    assertTrue(entity.getForce().yForce() == -10);
  }

  @Test
  public void adjustSpeedTest() {
    Rectangle hitbox = new Rectangle(2, 2, 2, 2);
    Force force = new Force(10, 0);
    ControllableEntity entity = new ControllableEntity(hitbox, force);
    // Moving 0
    entity.adjustSpeed();
    assertTrue(entity.getForce().xForce() == 9);
    entity.setOnGround(true);
    entity.adjustSpeed();
    assertTrue(entity.getForce().xForce() == 0);
    force = new Force(-10, 0);
    entity = new ControllableEntity(hitbox, force);
    entity.adjustSpeed();
    assertTrue(entity.getForce().xForce() == -9);
    entity.setOnGround(true);
    entity.adjustSpeed();
    assertTrue(entity.getForce().xForce() == 0);

    // Moving 1
    force = new Force(6, 0);
    entity = new ControllableEntity(hitbox, force);
    entity.changeMoving(1);
    entity.adjustSpeed();
    assertTrue(entity.getForce().xForce() == 5);
    entity.adjustSpeed();
    assertTrue(entity.getForce().xForce() == 5);
    force = new Force(4, 0);
    entity = new ControllableEntity(hitbox, force);
    entity.changeMoving(1);
    entity.adjustSpeed();
    assertTrue(entity.getForce().xForce() == 5);
    entity.adjustSpeed();
    assertTrue(entity.getForce().xForce() == 5);
    force = new Force(-4, 0);
    entity = new ControllableEntity(hitbox, force);
    entity.changeMoving(1);
    entity.setOnGround(true);
    entity.adjustSpeed();
    assertTrue(entity.getForce().xForce() == 5);

    // Moving -1
    force = new Force(-6, 0);
    entity = new ControllableEntity(hitbox, force);
    entity.changeMoving(-1);
    entity.adjustSpeed();
    assertTrue(entity.getForce().xForce() == -5);
    entity.adjustSpeed();
    assertTrue(entity.getForce().xForce() == -5);
    force = new Force(-4, 0);
    entity = new ControllableEntity(hitbox, force);
    entity.changeMoving(-1);
    entity.adjustSpeed();
    assertTrue(entity.getForce().xForce() == -5);
    entity.adjustSpeed();
    assertTrue(entity.getForce().xForce() == -5);
    force = new Force(4, 0);
    entity = new ControllableEntity(hitbox, force);
    entity.changeMoving(-1);
    entity.setOnGround(true);
    entity.adjustSpeed();
    assertTrue(entity.getForce().xForce() == -5);
  }

  @Test
  public void movingTest() {
    Rectangle hitbox = new Rectangle(2, 2, 2, 2);
    Force force = new Force(1, 1);
    ControllableEntity entity = new ControllableEntity(hitbox, force);
    assertTrue(entity.getMoving() == 0);
    entity.changeMoving(1);
    assertTrue(entity.getMoving() == 1);
    entity.changeMoving(2);
    assertTrue(entity.getMoving() == 1);
    entity.changeMoving(-1);
    assertTrue(entity.getMoving() == 0);
    entity.changeMoving(-1);
    assertTrue(entity.getMoving() == -1);
    entity.changeMoving(-2);
    assertTrue(entity.getMoving() == -1);
  }

  @Test
  public void onWallTest() {
    Rectangle hitbox = new Rectangle(2, 2, 2, 2);
    Force force = new Force(1, 1);
    ControllableEntity entity = new ControllableEntity(hitbox, force);
    assertTrue(entity.isOnWall() == 0);
    assertTrue(entity.setOnWall(2));
    assertTrue(entity.isOnWall() == 1);
    assertTrue(entity.setOnWall(-2));
    assertTrue(entity.isOnWall() == -1);
    assertFalse(entity.setOnWall(0));
    assertTrue(entity.isOnWall() == 0);
  }

  @Test
  public void phasingTest() {
    Rectangle hitbox = new Rectangle(2, 2, 2, 2);
    Force force = new Force(1, 1);
    ControllableEntity entity = new ControllableEntity(hitbox, force);
    assertFalse(entity.isPhasing());
    assertTrue(entity.setPhasing(true));
    assertTrue(entity.isPhasing());
    assertFalse(entity.setPhasing(false));
    assertFalse(entity.isPhasing());
  }
}
