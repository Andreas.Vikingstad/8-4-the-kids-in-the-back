package inf112.skeleton.app.model.entities;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Rectangle;

import org.junit.jupiter.api.Test;

public class EntityTest {

    @Test
    public void getHealthTest(){
        Rectangle hitbox = new Rectangle(2, 2, 2, 2);
        Entity entity = new Entity(hitbox);

        assertTrue(entity.getHealth() == -1);
    }

    @Test
    public void setHealthTest(){
        Rectangle hitbox = new Rectangle(2, 2, 2, 2);
        Entity entity = new Entity(hitbox);

        entity.setHealth(100);
        assertTrue(entity.getHealth() == 100);
    }

    @Test
    public void getDamageTest(){
        Rectangle hitbox = new Rectangle(2, 2, 2, 2);
        Entity entity = new Entity(hitbox);

        assertTrue(entity.getDamage() == 0);
    }
}
