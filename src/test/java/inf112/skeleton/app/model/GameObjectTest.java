package inf112.skeleton.app.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.awt.Rectangle;

public class GameObjectTest {
  
    
    @Test
    public void entitiesOverlapTest(){
        Rectangle hitbox = new Rectangle(2, 2, 2, 2);
        GameObject object = new GameObject(hitbox);
        Rectangle hitbox2 = new Rectangle(2, 2, 2, 2);
        GameObject object2 = new GameObject(hitbox);

        assertTrue(object.objectsOverlap(object2));
        
        hitbox2 = new Rectangle(25, 25, 25, 25);
        object2 = new GameObject(hitbox2);

        assertFalse(object.objectsOverlap(object2));

        hitbox = new Rectangle(0, 0, 25, 25);
        object = new GameObject(hitbox);
        hitbox2 = new Rectangle(24, 24, 25, 25);
        object2 = new GameObject(hitbox2);

        assertTrue(object.objectsOverlap(object2));
    }

    @Test
    public void getHitboxTest(){
        Rectangle hitbox = new Rectangle(2, 2, 2, 2);
        GameObject object = new GameObject(hitbox);

        assertTrue(object.getHitbox().equals(hitbox));
    }

    @Test
    public void getCharTest(){
        Rectangle hitbox = new Rectangle(2, 2, 2, 2);
        GameObject object = new GameObject(hitbox);
        assertThrows(NullPointerException.class, () -> {
          object.getChar();
        }, "Character not set");
    }
}
