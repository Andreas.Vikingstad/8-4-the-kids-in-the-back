package inf112.skeleton.app.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class GameStateTest {
 
  @Test
  public void gameStateTest() {
    GameState gameState = GameState.WELCOME_SCREEN;
    assertEquals(0, gameState.levelNumber);
    gameState = GameState.LEVEL_ONE;
    assertEquals(1, gameState.levelNumber);
    gameState = GameState.LEVEL_TWO;
    assertEquals(2, gameState.levelNumber);
    gameState = GameState.LEVEL_THREE;
    assertEquals(3, gameState.levelNumber);
    gameState = GameState.LEVEL_FOUR;
    assertEquals(4, gameState.levelNumber);
    gameState = GameState.LEVEL_FIVE;
    assertEquals(5, gameState.levelNumber);
    gameState = GameState.GAME_OVER;
    assertEquals(0, gameState.levelNumber);
    gameState = GameState.PAUSED;
    assertEquals(0, gameState.levelNumber);
    gameState = GameState.VICTORY;
    assertEquals(0, gameState.levelNumber);
  }
}
