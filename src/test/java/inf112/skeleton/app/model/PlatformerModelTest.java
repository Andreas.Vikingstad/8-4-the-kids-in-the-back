package inf112.skeleton.app.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;

import inf112.skeleton.app.model.entities.*;
import inf112.skeleton.app.model.level.PlatformerLevel;
import inf112.skeleton.app.model.level.PlatformerLevelFactory;
import inf112.skeleton.app.model.tiles.Tile;
import inf112.skeleton.eventBus.EventBus;
import inf112.skeleton.eventBus.SoundEvent;

public class PlatformerModelTest {

  EventBus eventBus = new EventBus();
  PlatformerLevelFactory factory = new PlatformerLevelFactory();
  PlatformerModel model;
  
  @Test
  public void getGameStateTest() {
    model = new PlatformerModel(eventBus, factory);
    assertEquals(model.getGameState(), GameState.WELCOME_SCREEN);
    assertNotEquals(model.getGameState(), GameState.GAME_OVER);
  }


  @Test
  public void movePlayerDownTest(){
    model = new PlatformerModel(eventBus, factory);
    model.startGame();
    assertTrue(model.movePlayerDown());
  } 

  @Test
  public void getPlayerTest() {
    model = new PlatformerModel(eventBus, factory);
    assertEquals(model.getPlayer(), null);
  }

  @Test
  public void getDimensionTest() {
    model = new PlatformerModel(eventBus, factory);
    Point dim = new Point(660, 400);
    Point notDim = new Point(100, 100);
    assertEquals(model.getDimension(), dim);
    assertNotEquals(model.getDimension(), notDim);
  }

  @Test
  public void getEntitiesTest() {
    model = new PlatformerModel(eventBus, factory);
    model.startGame();
    Rectangle box;

    ArrayList<Entity> entities = model.getEntities();
    assertTrue(entities.get(0) instanceof Decorations);
    box = new Rectangle(20, 60, 20, 20);
    assertEquals(entities.get(0).getHitbox(), box);
    assertTrue(entities.get(1) instanceof Decorations);
    box = new Rectangle(40, 60, 20, 20);
    assertEquals(entities.get(1).getHitbox(), box);
    assertTrue(entities.get(2) instanceof Decorations);
    box = new Rectangle(60, 60, 20, 20);
    assertEquals(entities.get(2).getHitbox(), box);
    assertTrue(entities.get(3) instanceof Guard);
    box = new Rectangle(360, 160, 20, 20);
    assertEquals(entities.get(3).getHitbox(), box);
    assertTrue(entities.get(4) instanceof SpeedBoost);
    box = new Rectangle(120, 180, 20, 20);
    assertEquals(entities.get(4).getHitbox(), box);
    assertTrue(entities.get(5) instanceof Decorations);
    box = new Rectangle(200, 180, 20, 20);
    assertEquals(entities.get(5).getHitbox(), box);
    assertTrue(entities.get(6) instanceof Decorations);
    box = new Rectangle(220, 180, 20, 20);
    assertEquals(entities.get(6).getHitbox(), box);
    assertTrue(entities.get(7) instanceof Decorations);
    box = new Rectangle(240, 180, 20, 20);
    assertEquals(entities.get(7).getHitbox(), box);
    assertTrue(entities.get(8) instanceof Decorations);
    box = new Rectangle(480, 260, 20, 20);
    assertEquals(entities.get(8).getHitbox(), box);
    assertTrue(entities.get(9) instanceof Decorations);
    box = new Rectangle(100, 340, 20, 20);
    assertEquals(entities.get(9).getHitbox(), box);
    assertTrue(entities.get(10) instanceof Decorations);
    box = new Rectangle(120, 340, 20, 20);
    assertEquals(entities.get(10).getHitbox(), box);
    assertTrue(entities.get(11) instanceof Janitor);
    box = new Rectangle(480, 340, 20, 20);
    assertEquals(entities.get(11).getHitbox(), box);
    assertTrue(entities.get(12) instanceof Door);
    box = new Rectangle(40, 360, 20, 20);
    assertEquals(entities.get(12).getHitbox(), box);
    assertTrue(entities.get(13) instanceof Decorations);
    box = new Rectangle(100, 360, 20, 20);
    assertEquals(entities.get(13).getHitbox(), box);
    assertTrue(entities.get(14) instanceof Decorations);
    box = new Rectangle(120, 360, 20, 20);
    assertEquals(entities.get(14).getHitbox(), box);
    assertTrue(entities.get(15) instanceof Decorations);
    box = new Rectangle(260, 360, 20, 20);
    assertEquals(entities.get(15).getHitbox(), box);
    assertTrue(entities.get(16) instanceof Decorations);
    box = new Rectangle(280, 360, 20, 20);
    assertEquals(entities.get(16).getHitbox(), box);
    assertTrue(entities.get(17) instanceof Decorations);
    box = new Rectangle(300, 360, 20, 20);
    assertEquals(entities.get(17).getHitbox(), box);
    assertTrue(entities.get(18) instanceof Decorations);
    box = new Rectangle(600, 360, 20, 20);
    assertEquals(entities.get(18).getHitbox(), box);
    assertTrue(entities.get(19) instanceof Decorations);
    box = new Rectangle(620, 360, 20, 20);
    assertEquals(entities.get(19).getHitbox(), box);
    assertTrue(entities.size() == 20);

  }

  @Test
  public void jumpPlayertest(){
    model = new PlatformerModel(eventBus, factory);
    model.startGame();
    assertFalse(model.jumpPlayer());
  }
  
  @Test
  public void movePlayerRightTest(){
    model = new PlatformerModel(eventBus, factory);
    model.startGame();
    assertTrue(model.movePlayerRight(true));
    assertFalse(model.movePlayerRight(false));
  }

  @Test
  public void movePlayerLeftTest(){
    model = new PlatformerModel(eventBus, factory);
    model.startGame();
    assertTrue(model.movePlayerLeft(true));
    assertFalse(model.movePlayerLeft(false));
  }

  @Test
  public void changeSkinTest(){
    model = new PlatformerModel(eventBus, factory);
    model.changeSkin(1);
    assertEquals(model.getPlayerSpriteInt(), 1);
    model.changeSkin(2);
    assertEquals(model.getPlayerSpriteInt(), 2);
  }

  @Test
  public void getTilesTest() {
    model = new PlatformerModel(eventBus, factory);
    ArrayList<Tile> tiles = new ArrayList<Tile>();
    ArrayList<Tile> modelTiles = model.getTiles();
    assertNotEquals(tiles, modelTiles);
    String level = """
    #################################
    #-------------------------------#
    #-S-----------------------------#
    #-------------------------------#
    #######--####-------------------#
    #-----#--#----------------------#
    #-----####----------------------#
    #-------------------------------#
    #-----------------G-------------#
    #-------------------------------#
    ####################------------#
    #-------------------------------#
    #-------------------------------#
    #-------------------------------#
    #----------------------#######--#
    #-------------------------------#
    #-------------------#-----------#
    #-------##----------#---J-------#
    #-D-----##----------#-----------#
    #################################
    """;
    PlatformerLevel lvl = factory.createLevelFromString(level);
    tiles = lvl.getTiles();
    for (int i = 0; i < tiles.size(); i++) {
      assertEquals(tiles.get(i).getHitbox(), modelTiles.get(i).getHitbox());
    }
    assertEquals(tiles.size(), modelTiles.size());
  }
    
  @Test
  public void gePlayerSpriteInt(){
    model = new PlatformerModel(eventBus, factory);
    model.changeSkin(2);
    assertEquals(model.getPlayerSpriteInt(), 2);
  } 
    
 @Test
  public void restartGameTest() {
    model = new PlatformerModel(eventBus, factory);
    model.startGame();
    model.restartGame();
    assertEquals(GameState.LEVEL_ONE, model.getGameState(), "Game should be reset to LEVEL_ONE");
  }

  @Test 
  public void pauseGameTest() {
    model = new PlatformerModel(eventBus, factory);
    model.startGame();
    model.pauseGame();
    assertEquals(GameState.PAUSED, model.getGameState(), "Game should now be paused");
  }

  @Test
  public void unPauseTest(){
    model = new PlatformerModel(eventBus, factory);
    model.startGame();
    GameState gameState = model.getGameState();
    model.pauseGame();
    model.unPause();
    assertEquals(model.getGameState(), gameState);
  }

  @Test
  public void startGameTest() {
    model = new PlatformerModel(eventBus, factory);
    model.startGame();
    assertEquals(model.getGameState(), GameState.LEVEL_ONE);
  }
  
  @Test
  public void soundTest() {
    model = new PlatformerModel(eventBus, factory);
    model.startGame();
    eventBus.post(new SoundEvent("hello"));
  }
  
}
