package inf112.skeleton.app.model.controller;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.awt.event.KeyEvent;

import org.junit.jupiter.api.Test;

import inf112.skeleton.app.controller.PlatformerController;
import inf112.skeleton.app.model.GameState;
import inf112.skeleton.app.model.PlatformerModel;
import inf112.skeleton.app.model.level.PlatformerLevelFactory;
import inf112.skeleton.app.view.PlatformerView;
import inf112.skeleton.app.view.LevelThemes.DefaultLevelTheme;
import inf112.skeleton.eventBus.EventBus;


public class PlatformerControllerTest {

  
  EventBus eventBus = new EventBus();
  PlatformerModel model = new PlatformerModel(eventBus, new PlatformerLevelFactory());
  PlatformerView view = new PlatformerView(model, eventBus, new DefaultLevelTheme("default"));
  PlatformerController controller = new PlatformerController(model, view, eventBus);


  @Test
  public void controllerTest() {
    
    // Pressed keys
    KeyEvent spaceP = new KeyEvent(view, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0, KeyEvent.VK_SPACE, ' ');
    KeyEvent downP = new KeyEvent(view, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0, KeyEvent.VK_DOWN, ' ');
    KeyEvent leftP = new KeyEvent(view, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0, KeyEvent.VK_LEFT, ' ');
    KeyEvent rightP = new KeyEvent(view, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0, KeyEvent.VK_RIGHT, ' ');
    KeyEvent pP = new KeyEvent(view, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0, KeyEvent.VK_P, ' ');
    KeyEvent oneP = new KeyEvent(view, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0, KeyEvent.VK_1, ' ');
    KeyEvent twoP = new KeyEvent(view, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0, KeyEvent.VK_2, ' ');
    KeyEvent threeP = new KeyEvent(view, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0, KeyEvent.VK_3, ' ');
    KeyEvent fourP = new KeyEvent(view, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0, KeyEvent.VK_4, ' ');
    KeyEvent fiveP = new KeyEvent(view, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0, KeyEvent.VK_5, ' ');

    // Released keys
    KeyEvent spaceR = new KeyEvent(view, KeyEvent.KEY_RELEASED, System.currentTimeMillis(), 0, KeyEvent.VK_SPACE, ' ');
    KeyEvent leftR = new KeyEvent(view, KeyEvent.KEY_RELEASED, System.currentTimeMillis(), 0, KeyEvent.VK_LEFT, ' ');
    KeyEvent rightR = new KeyEvent(view, KeyEvent.KEY_RELEASED, System.currentTimeMillis(), 0, KeyEvent.VK_RIGHT, ' ');

    assertTrue(model.getGameState() == GameState.WELCOME_SCREEN);
    controller.keyReleased(spaceR);
    assertTrue(model.getGameState() == GameState.LEVEL_ONE);
    controller.keyPressed(spaceP);
    controller.keyPressed(downP);
    controller.keyPressed(leftP);
    controller.keyReleased(leftR);
    controller.keyPressed(rightP);
    controller.keyReleased(rightR);
    controller.keyPressed(pP);
    assertTrue(model.getGameState() == GameState.PAUSED);
    controller.keyPressed(oneP);
    controller.keyPressed(pP);
    assertTrue(model.getGameState() == GameState.PAUSED);
    controller.keyPressed(twoP);
    controller.keyPressed(pP);
    assertTrue(model.getGameState() == GameState.PAUSED);
    controller.keyPressed(threeP);
    controller.keyPressed(pP);
    assertTrue(model.getGameState() == GameState.PAUSED);
    controller.keyPressed(fourP);
    controller.keyPressed(pP);
    assertTrue(model.getGameState() == GameState.PAUSED);
    controller.keyPressed(fiveP);
    controller.keyPressed(pP);
    assertTrue(model.getGameState() == GameState.PAUSED);
    controller.keyPressed(pP);
  }
  
}
