package inf112.skeleton.app.model.level;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import inf112.skeleton.app.model.entities.Entity;
import inf112.skeleton.app.model.tiles.Tile;

public class PlatformerLevelTest {

  ArrayList<Tile> tiles = new ArrayList<>();
  Point startPos = new Point(0, 0);
  Point endPos = new Point(1, 1);
  ArrayList<Entity> entities = new ArrayList<>();

  PlatformerLevel level = new PlatformerLevel(tiles, startPos, entities);

  @Test
  public void getStarPositionTest() {
    assertEquals(startPos, level.getStarPosition());
    assertNotEquals(endPos, level.getStarPosition());;
  }

  @Test
  public void getTilesTest() {
    assertEquals(tiles, level.getTiles());
    ArrayList<Tile> notTiles = new ArrayList<>();
    notTiles.addAll(tiles);
    notTiles.add(new Tile(new Rectangle(0, 0, 20, 20)));
    assertNotEquals(notTiles, level.getTiles());
  }

  @Test
  public void getEntitiesTest() {
    assertEquals(entities, level.getEntities());
    ArrayList<Entity> notEntities = new ArrayList<>();
    notEntities.addAll(entities);
    notEntities.add(new Entity(new Rectangle(0, 0, 20, 20)));
    assertNotEquals(notEntities, level.getEntities());
  }

  @Test
  public void getDimensionsTest() {
    assertEquals(new Point(0, 0), level.getDimensions());
    tiles.add(new Tile(new Rectangle(0, 0, 20, 20)));
    tiles.add(new Tile(new Rectangle(20, 0, 20, 20)));
    tiles.add(new Tile(new Rectangle(0, 0, 20, 20)));
    tiles.add(new Tile(new Rectangle(20, 20, 20, 20)));
    assertNotEquals(new Point(1, 1), level.getDimensions());
    assertEquals(new Point(40, 40), level.getDimensions());
  }

}
