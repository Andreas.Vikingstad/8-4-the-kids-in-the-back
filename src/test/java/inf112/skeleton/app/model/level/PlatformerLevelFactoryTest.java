package inf112.skeleton.app.model.level;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.awt.Point;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import inf112.skeleton.app.model.tiles.*;

public class PlatformerLevelFactoryTest {

  PlatformerLevelFactory factory = new PlatformerLevelFactory();

  @Test
  public void createLevelTest() {
    ArrayList<String> level = new ArrayList<>();
    level.add("KDHC");
    level.add("JSGL");
    level.add("___Q");
    level.add("##VW");
    PlatformerLevel platformerLevel = factory.createLevel(level);
    ArrayList<Tile> tiles = platformerLevel.getTiles();
    assertEquals(new Point(0, 40), tiles.get(0).getHitbox().getLocation());
    assertEquals(new Point(20, 40), tiles.get(1).getHitbox().getLocation());
    assertEquals(new Point(40, 40), tiles.get(2).getHitbox().getLocation());
    assertEquals(new Point(0, 60), tiles.get(3).getHitbox().getLocation());
    assertEquals(new Point(20, 60), tiles.get(4).getHitbox().getLocation());

    ArrayList<String> level2 = new ArrayList<>();
    level2.add("###");
    level2.add("#R#");
    level2.add("###");
    level2.add("#---");
    assertThrows(IllegalArgumentException.class, () -> factory.createLevel(level2));

    ArrayList<String> level3 = new ArrayList<>();
    level3.add("-##");
    level3.add("#SO");
    level3.add("---");
    platformerLevel = factory.createLevel(level3);
    tiles = platformerLevel.getTiles();
    assertEquals(new Point(20, 0), tiles.get(0).getHitbox().getLocation());
    assertEquals(new Point(40, 0), tiles.get(1).getHitbox().getLocation());
    assertEquals(new Point(0, 20), tiles.get(2).getHitbox().getLocation());

}
    
  @Test
  public void createLevelFromStringTest() {
    PlatformerLevel level = factory.createLevelFromString("###-\n#S#-\n###-\n#---");
    ArrayList<Tile> tiles = level.getTiles();
    assertEquals(new Point(0, 0), tiles.get(0).getHitbox().getLocation());
    assertEquals(new Point(20, 0), tiles.get(1).getHitbox().getLocation());
    assertEquals(new Point(40, 0), tiles.get(2).getHitbox().getLocation());
    assertEquals(new Point(0, 20), tiles.get(3).getHitbox().getLocation());
    assertEquals(new Point(40, 20), tiles.get(4).getHitbox().getLocation());
    assertEquals(new Point(0, 40), tiles.get(5).getHitbox().getLocation());
    assertEquals(new Point(20, 40), tiles.get(6).getHitbox().getLocation());
    assertEquals(new Point(40, 40), tiles.get(7).getHitbox().getLocation());

    level = factory.createLevelFromString("###-\n#S#-\n###-\n#---");
    tiles = level.getTiles();
    assertEquals(new Point(0, 0), tiles.get(0).getHitbox().getLocation());
    assertEquals(new Point(20, 0), tiles.get(1).getHitbox().getLocation());
    assertEquals(new Point(40, 0), tiles.get(2).getHitbox().getLocation());
    assertEquals(new Point(0, 20), tiles.get(3).getHitbox().getLocation());
    assertEquals(new Point(40, 20), tiles.get(4).getHitbox().getLocation());
    assertEquals(new Point(0, 40), tiles.get(5).getHitbox().getLocation());
    assertEquals(new Point(20, 40), tiles.get(6).getHitbox().getLocation());
    assertEquals(new Point(40, 40), tiles.get(7).getHitbox().getLocation());
    assertEquals(new Point(0, 60), tiles.get(8).getHitbox().getLocation());

    level = factory.createLevelFromString("##\n#S");
    tiles = level.getTiles();
    assertEquals(new Point(0, 0), tiles.get(0).getHitbox().getLocation());
    assertEquals(new Point(20, 0), tiles.get(1).getHitbox().getLocation());
    assertEquals(new Point(0, 20), tiles.get(2).getHitbox().getLocation());
        
    assertThrows(IllegalArgumentException.class, () -> factory.createLevelFromString("###\n###\n#-#"));
    }
}
