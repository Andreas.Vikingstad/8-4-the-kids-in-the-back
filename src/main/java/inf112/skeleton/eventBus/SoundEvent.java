package inf112.skeleton.eventBus;

/**
 * An event that signals that a sound is to be played.
 * 
 * @param soundName is the name of the sound.
 */
public record SoundEvent(String soundName) implements Event {
  
}
