package inf112.skeleton.eventBus;

/**
 * An event that calls for the view to repaint
 */
public record RepaintEvent() implements Event {}
