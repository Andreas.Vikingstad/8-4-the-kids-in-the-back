package inf112.skeleton.eventBus;

import java.util.List;
import java.util.ArrayList;

// Evenbus inspirert av kursnotatene INF101 https://inf101.ii.uib.no/notat/eventbuss/  

public class EventBus {

  private List<EventHandler> eventHandlers = new ArrayList<>();

  /**
   * Registers listeners for the eventbus
   * 
   * @param eventHandler the method to call when events are registered
   */
  public void register(EventHandler eventHandler) {
    this.eventHandlers.add(eventHandler);
  }

  /**
   * Registeres an event and posts it for listeners to react
   * 
   * @param event the event that is registered
   */
  public void post(Event event) {
    for (EventHandler eventHandler : this.eventHandlers) {
      eventHandler.handle(event);
    }
  }
}
