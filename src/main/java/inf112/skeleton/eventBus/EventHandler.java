package inf112.skeleton.eventBus;

public interface EventHandler {
  /**
   * Handles a task asked for by the listener
   * 
   * @param event the event that is asked to be handled
   */
  void handle(Event event);
}
