package inf112.skeleton.app.view.LevelThemes;

import java.util.ArrayList;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.InputStream;

public class DefaultLevelTheme implements LevelTheme {
  HashMap<Character, ArrayList<BufferedImage>> sprites = new HashMap<Character, ArrayList<BufferedImage>>();
  HashMap<String, Clip> sounds = new HashMap<String, Clip>();
  
  /**
   * Constructor for DefaultLevelTheme
   * @param texturePack The texture pack to use
   */
  public DefaultLevelTheme(String texturePack) {
    String path = "/inf112/skeleton/app/model/";

    String packPath =  path + "sprites/" + texturePack + "/";
    loadImage('G', packPath + "enemyAnimation1.png");
    loadImage('J', packPath + "enemyAnimation2.png");
    loadImage('H', packPath + "energyBoost.png");
    loadImage('D', packPath + "door1-Sheet.png");
    loadImage('K', packPath + "key.png");
    loadImage('B', packPath + "quickShades.png");
    loadImage('C', packPath + "computer.png");
    loadImage('F', packPath + "decorationSheet.png");
    loadImage('#', packPath + "bricks/Bricks1.png");
    loadImage('-', packPath + "bricks/Bricks2.png");
    loadImage('_', packPath + "bricks/Bricks3.png");


    String playerPath = path + "sprites/" + "player/";
    loadImage('1', playerPath + "characterAnimation1.png");
    loadImage('2', playerPath + "characterAnimation2.png");
    loadImage('3', playerPath + "characterAnimation3.png");
    loadImage('4', playerPath + "characterAnimation4.png");
    loadImage('5', playerPath + "characterAnimation5.png");

    String soundPath = "/inf112/skeleton/app/model/sounds/";
    loadSound("healthBoost", soundPath + "healthBoost.wav");
    loadSound("hurt1", soundPath + "hurtSound1.wav");
    loadSound("hurt2", soundPath + "hurtSound2.wav");
    loadSound("lost", soundPath + "wompWomp.wav");
    loadSound("locked", soundPath + "knockOnDoor.wav"); 
  }

  /**
   * Loads a spritesheet from a path defined by the path parameter and stores it in the sprites hashmap
   * @param c The character that the spritesheet is associated with
   * @param path The path to the spritesheet
   */
  private void loadImage(char c, String path) {
    BufferedImage spriteSheet = null;
    InputStream is = getClass().getResourceAsStream(path);
    System.out.println("Loading sprite with char: " + c);
    System.out.println("Loading image at path: " + path);
    try {
      spriteSheet = ImageIO.read(is);
    } catch (Exception e) {}
    if (spriteSheet.getWidth() <= 20 && spriteSheet.getHeight() <= 20) {
      ArrayList<BufferedImage> sprite = new ArrayList<BufferedImage>();
      sprite.add(spriteSheet);
      sprites.put(c, sprite);
      return;
    }
    ArrayList<BufferedImage> sprite = new ArrayList<BufferedImage>();
    for (int i = 0; i < spriteSheet.getWidth() / 20; i++) {
      for (int j = 0; j < spriteSheet.getHeight() / 20; j++) {
        sprite.add(spriteSheet.getSubimage(i * 20, j * 20, 20, 20));
      }
    }
    sprites.put(c, sprite);
  }

  /**
   * Loads a sound from a path defined by the path parameter and stores it in the sounds hashmap
   * @param soundName The name of the sound
   * @param path The path to the sound
   */
  private void loadSound(String soundName, String path) {
    InputStream is = getClass().getResourceAsStream(path);
    Clip clip = null;
    try {
      clip = AudioSystem.getClip();
      clip.open(AudioSystem.getAudioInputStream(is));
    } catch (Exception e) {}
    sounds.put(soundName, clip);
  }

  @Override
  public Image getSprite(char c, int spriteNumber) {
    spriteNumber = spriteNumber % this.sprites.get(c).size();
    ArrayList<BufferedImage> sprites = this.sprites.get(c);
    return sprites.get(spriteNumber);
  }

  @Override
  public Image getPlayerSprite(int charatcerNumber) {
    char c = (char) (charatcerNumber + '0');
    return getSprite(c, 0);
  }

  @Override
  public Image getBackgroundSprite() {
    return getSprite('-', 0);
  }

  @Override
  public boolean playSound(String soundName) {
    if (!sounds.containsKey(soundName)) {
      return false;
    }
    Clip clip = sounds.get(soundName);
    clip.setFramePosition(0);
    clip.start();
    return true;
  }
}
