package inf112.skeleton.app.view.LevelThemes;

import java.awt.Image;

public interface LevelTheme {

  /**
   * Returns the sprite of the given Character
   * @param c The entity to get the sprite of
   * @param spriteNumber the frame of the sprite if it is an entity with multiple sprites or the level of the sprite if it is a tile
   */
  public Image getSprite(char c, int spriteNumber);

  /**
   * Returns the sprite of the player
   * @param c 
   * @return the sprite of the player
   */
  public Image getPlayerSprite(int charatcerNumber);

  /**
   * Returns the background sprite
   * @return the background sprite
   */
  public Image getBackgroundSprite();

  /**
   * Plays a sound
   * @param soundName The name of the sound to play
   */
  public boolean playSound(String soundName);
}
