package inf112.skeleton.app.view;

import java.awt.Point;
import java.util.ArrayList;

import inf112.skeleton.app.model.GameState;
import inf112.skeleton.app.model.entities.Entity;
import inf112.skeleton.app.model.entities.Player;
import inf112.skeleton.app.model.tiles.Tile;

public interface ViewablePlatformerModel {

    /**
     * Gives the player of the level
     * 
     * @return the player
     */
    Player getPlayer();

    /**
     * Gives the current gamestate
     * 
     * @return the current GameState
     */
    GameState getGameState();

    /**
     * Gives the entities of the level as a set
     * 
     * @return a set of the entities
     */
    ArrayList<Entity> getEntities();

    /**
     * Gives the tiles of the level as a set
     * 
     * @return a set of the tiles
     */
    ArrayList<Tile> getTiles();

    /**
     * Gets the dimension of the level as an array of two integers
     * 
     * @return the dimension of the level as a position, where x is width and y is height
     */
    Point getDimension();

    /**
     * Returns the sprite of the given Character
     * @return the sprite of the given Character
     */
    int getPlayerSpriteInt();
}
