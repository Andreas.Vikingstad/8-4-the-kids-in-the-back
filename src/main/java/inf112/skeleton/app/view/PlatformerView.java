package inf112.skeleton.app.view;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Graphics;

import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Dimension;

import inf112.skeleton.app.model.GameObject;
import inf112.skeleton.app.model.GameState;
import inf112.skeleton.app.model.entities.Door;
import inf112.skeleton.app.model.entities.Decorations;
import inf112.skeleton.app.view.LevelThemes.DefaultLevelTheme;
import inf112.skeleton.app.view.LevelThemes.LevelTheme;
import inf112.skeleton.eventBus.*;

import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PlatformerView extends JPanel {

  private ViewablePlatformerModel model;
  private DefaultLevelTheme levelTheme;
  private static final List<Integer> spriteNums = Arrays.asList(1,2,3,4,5);
  private static final Font defaultFont = new Font("Arial", Font.BOLD, 25); 
  private static final Color defaultFontColor = Color.WHITE;
  /**
   * Constructor for PlatformerView
   * 
   * @param model    The model to be viewed
   * @param eventBus The eventbus to be used
   * @param theme    The theme to be used
   */
  public PlatformerView(ViewablePlatformerModel model, EventBus eventBus, DefaultLevelTheme levelTheme) {
    this.model = model;
    eventBus.register(this::repaintView);
    this.levelTheme = levelTheme;

    this.setBackground(Color.BLACK);
    this.setFocusable(true);
    this.setPreferredSize(new Dimension(800,800));
  }

  private void repaintView(Event event) {
    if (event instanceof RepaintEvent) {
      this.repaint();
    }
    if (event instanceof SoundEvent) {
      event = (SoundEvent) event;
      levelTheme.playSound(((SoundEvent) event).soundName());
    }
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGame(g2);
  }

  private void drawGame(Graphics2D g2) {

    double x = 0;
    double y = 0;
    double width = this.getWidth();
    double height = this.getHeight();

    Rectangle2D box = new Rectangle2D.Double(x, y, width, height);

    Shape shape = box;

    g2.setColor(Color.BLACK);
    g2.fill(shape);
    g2.draw(shape);
    g2.setFont(defaultFont);

    if (model.getGameState() == GameState.WELCOME_SCREEN) {
      drawStartScreen(g2);
    } 
    else if (model.getGameState() == GameState.GAME_OVER) {
      g2.setColor(defaultFontColor);
      drawCenteredString(g2, "Game over", width / 2, height / 2);
      g2.setFont(new Font("Arial", Font.BOLD, 15));
      drawCenteredString(g2, "Press r to restart", width / 2, height / 5 * 3);
    } 
    else if (model.getGameState() == GameState.VICTORY) {
      g2.setColor(defaultFontColor);
      drawCenteredString(g2, "Victory", width / 2, height / 2);
      g2.setFont(new Font("Arial", Font.BOLD, 15));
      drawCenteredString(g2, "Press r to restart", width / 2, height / 5 * 3);
    }
    else if (model.getGameState() == GameState.PAUSED) {
      drawSkins(g2);
    }
    else {
      int visibleWidth = 300;
      int visibleHeight = 300;

      Rectangle p = model.getPlayer().getHitbox();

      int pCenterX = (int) p.getCenterX();
      int pCenterY = (int) p.getCenterY();

      int cameraX = pCenterX - visibleWidth / 2;
      int cameraY = pCenterY - visibleHeight / 2;

      int worldWidth = model.getDimension().x;
      int worldHeigth = model.getDimension().y;

      cameraX = Math.max(cameraX, 0);
      cameraY = Math.max(cameraY, 0);
      cameraX = Math.min(cameraX, worldWidth - visibleWidth);
      cameraY = Math.min(cameraY, worldHeigth - visibleHeight);

      double scaleX = getWidth() / (double) visibleWidth;
      double scaleY = getHeight() / (double) visibleHeight;
      g2.scale(scaleX, scaleY);

      g2.translate(-cameraX, -cameraY);

      // Tegner bakgrunnen
        for (int i = 0; i < model.getDimension().x/20; i++) {
        for (int j = 0; j < model.getDimension().y/20; j++) {
          g2.drawImage(levelTheme.getBackgroundSprite(), i*20, j*20, null);
        }
      }
      // Tegner brettet
      ArrayList<GameObject> objects = new ArrayList<GameObject>();
      objects.addAll(model.getTiles());
      drawGameObjects(g2, objects, levelTheme);
      // Tegner entities
      objects = new ArrayList<GameObject>();
      objects.addAll(model.getEntities());
      drawGameObjects(g2, objects, levelTheme);
      // Tegner spilleren
      Rectangle2D pHitbox = model.getPlayer().getHitbox();
      Image pSprite = levelTheme.getPlayerSprite(model.getPlayerSpriteInt());
      g2.drawImage(pSprite, (int) pHitbox.getX(), (int) pHitbox.getY(), null);
      // Tegner livet til spilleren oppe i venstre hjørne
      // og beveger seg rundt brettet sammen med kameraet
      g2.setColor(defaultFontColor);
      g2.drawString("Lives: " + model.getPlayer().getHealth() +"/3", cameraX + 5, cameraY + 22);
    }
  }

  private static void drawStartScreen(Graphics2D g2) {
    double width = g2.getClipBounds().getWidth();
    double height = g2.getClipBounds().getHeight();
    g2.setColor(defaultFontColor);
    int spacing = 15;
    drawCenteredString(g2, "Welcome to the game", width / 2, height / 2 - spacing);
    drawCenteredString(g2, "Press any button to continue", width / 2, height / 2 + spacing);
    g2.setFont(new Font("Arial", Font.BOLD, 15));
    drawCenteredString(g2, "Use arrow keys for horisontal movement,", width / 2, height / 2 + spacing * 3);
    drawCenteredString(g2, "space for jumping and p for pause", width / 2, height / 2 + spacing * 4);
  }

  private void drawGameObjects(Graphics2D g2, ArrayList<GameObject> objects, LevelTheme levelTheme) {
    for (GameObject object : objects) {
      Rectangle2D box = object.getHitbox();
      int x = ((int)box.getX());
      int y = ((int)box.getY());
      switch (object.getChar()) {
        case 'H':
          g2.drawImage(levelTheme.getSprite('H', 0), x, y, null);
          break;
        case 'K':
          g2.drawImage(levelTheme.getSprite('K', 0), x, y, null);
          break;
        case 'D':
          object = (Door) object;
          if (((Door) object).isLocked()) {
            g2.drawImage(levelTheme.getSprite('D', 1), x, y, null);
          } else {
            g2.drawImage(levelTheme.getSprite('D', 0), x, y, null);
          }
          break;
        case 'F':
          if (((Decorations) object).getType().equals("table")) {
            g2.drawImage(levelTheme.getSprite('F', 0), x, y, null);
          }
          if (((Decorations) object).getType().equals("green_chair")) {
            g2.drawImage(levelTheme.getSprite('F', 1), x, y, null);
          }
          if (((Decorations) object).getType().equals("blue_chair")) {
            g2.drawImage(levelTheme.getSprite('F', 2), x, y, null);
          }
          if (((Decorations) object).getType().equals("locker")) {
            g2.drawImage(levelTheme.getSprite('F', 3), x, y, null);
          }
          break;
        case 'J':
          g2.drawImage(levelTheme.getSprite('J', 0), x, y, null);
          break;
        case 'B':
          g2.drawImage(levelTheme.getSprite('B', 0), x, y, null);
          break;
        case 'G':
          g2.drawImage(levelTheme.getSprite('G', 0), x, y, null);
          break;
        case 'C':
          g2.drawImage(levelTheme.getSprite('C', 0), x, y, null);
          break;
        case '#':
          g2.drawImage(levelTheme.getSprite('#', 0), x, y, null);
          break;
        case '_':
          g2.drawImage(levelTheme.getSprite('_', 0), x, y, null);
          break;
        default:
          throw new IllegalArgumentException("Invalid object symbol: " + object.getChar());
      }
    }
  }

  private static void drawCenteredString(Graphics2D g2, String s, double x, double y) {
    FontMetrics metrics = g2.getFontMetrics();
    g2.drawString(s, Math.round(x - metrics.stringWidth(s) / 2), Math.round(y + metrics.getHeight() / 4));
  }

  private void drawSkins(Graphics2D g2) {
    int screenWidth = getWidth();
    int totalWidth = 0;
    int spacing = 40;
    
    totalWidth += spriteNums.size() * 40;
    totalWidth += (spriteNums.size() - 1) * spacing;

    int x = (screenWidth - totalWidth) / 2;
    x += spacing/2;
    int y = getHeight()/2;

    int i = 1;

    for (Integer num : spriteNums){
      Image image = levelTheme.getPlayerSprite(i);
      int newWidth = image.getWidth(null) * 2;
      int newHeight = image.getHeight(null) * 2;
      Image scaledImage = image.getScaledInstance(newWidth, newHeight, Image.SCALE_DEFAULT);
      g2.drawImage(scaledImage, x, y, null);
      g2.setColor(Color.WHITE);
      g2.drawString(""+num, x+10, getHeight()/10*7);
      x += image.getWidth(null) + spacing;
      i++;
    }
  }
}
