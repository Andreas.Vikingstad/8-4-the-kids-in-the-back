package inf112.skeleton.app.controller;

import inf112.skeleton.app.model.GameState;

public interface ControllablePlatformerModel {

    /**
     * A method which returns the state of the game.
     * @return ActiveGame, GameOver, StartMenu etc
     */
    GameState getGameState();

    /**
     * Starts a new game.
     */
    void startGame();

    /**
     * Moves the player to the left.
     * @return true if the player was moved, false otherwise
     */
    boolean movePlayerLeft(boolean moveLeft);

    /**
     * Moves the player to the right.
     * @return true if the player was moved, false otherwise
     */
    boolean movePlayerRight(boolean moveRight);

    /**
     * Makes the player jump.
     * @return true if the player jumped, false otherwise
     */
    boolean jumpPlayer();

    /**
     * Moves the player down.
     * @return true if the player was phased, false otherwise
     */
    boolean movePlayerDown();

    /**
     * pauses the game
     */
    void pauseGame();

    /**
     * Resumes the game
     */
    void unPause();

    /**
     * Changes the skin of the player
     */
    void changeSkin(int i);

    /**
     * restarts the game
     */
    void restartGame();
}
