package inf112.skeleton.app.controller;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import inf112.skeleton.app.model.GameState;
import inf112.skeleton.app.view.PlatformerView;
import inf112.skeleton.eventBus.EventBus;
import inf112.skeleton.eventBus.RepaintEvent;



public class PlatformerController implements KeyListener {
  
  private ControllablePlatformerModel model;
  private EventBus eventBus;
  private boolean moveLeft = false;
  private boolean moveRight = false;
  
  /**
   * Constructor for PlatformerController
   * @param model The model to be controlled
   * @param view The view to be controlled
   * @param eventBus The eventbus to be used
   */
  public PlatformerController(ControllablePlatformerModel model, PlatformerView view, EventBus eventBus) {
    this.model = model;
    this.eventBus = eventBus;
    
    view.setFocusable(true);
    view.addKeyListener(this);
  }
  
  @Override
  public void keyTyped(KeyEvent e) {
    // Do nothing
  }
  
  @Override
  public void keyPressed(KeyEvent e) {
    if (model.getGameState() != GameState.WELCOME_SCREEN
    && model.getGameState() != GameState.PAUSED){
      if (e.getKeyCode() == KeyEvent.VK_P) {
        model.pauseGame();
      }
      
      else if (e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == 'A') {
        if (!moveLeft) {
          moveLeft = true;
          model.movePlayerLeft(true);
        }
      }
      else if (e.getKeyCode() == KeyEvent.VK_RIGHT || e.getKeyCode() == 'D') {
        if (!moveRight) {
          moveRight = true;
          model.movePlayerRight(true);
        }
      }
      else if (e.getKeyCode() == KeyEvent.VK_SPACE || e.getKeyCode() == 'W') {
        model.jumpPlayer();
      }
      else if (e.getKeyCode() == KeyEvent.VK_DOWN || e.getKeyCode() == 'S') {
        model.movePlayerDown();
      }
    }
    else if (model.getGameState() == GameState.PAUSED) {
      if (e.getKeyCode() == KeyEvent.VK_1) {
        model.changeSkin(1);
        model.unPause();
      }
      else if (e.getKeyCode() == KeyEvent.VK_2) {
        model.changeSkin(2);
        model.unPause();
      }
      else if (e.getKeyCode() == KeyEvent.VK_3) {
        model.changeSkin(3);
        model.unPause();
      }
      else if (e.getKeyCode() == KeyEvent.VK_4) {
        model.changeSkin(4);
        model.unPause();
      }
      else if (e.getKeyCode() == KeyEvent.VK_5) {
        model.changeSkin(5);
        model.unPause();
      }
      else if (e.getKeyCode() == KeyEvent.VK_P) {
        model.unPause();
      }
    }
    this.eventBus.post(new RepaintEvent());
    
  }
  
  @Override
  public void keyReleased(KeyEvent e) {
    if (model.getGameState() == GameState.WELCOME_SCREEN) {
      model.startGame();
    } else {
      if (e.getKeyCode() == KeyEvent.VK_R && (model.getGameState() == GameState.GAME_OVER || model.getGameState() == GameState.VICTORY)) {
      model.restartGame();
      }
      if (e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == 'A') {
        moveLeft = false;
        model.movePlayerLeft(false);
      }
      if (e.getKeyCode() == KeyEvent.VK_RIGHT || e.getKeyCode() == 'D') {
        moveRight = false;
        model.movePlayerRight(false);
      }
    }
  }
}
