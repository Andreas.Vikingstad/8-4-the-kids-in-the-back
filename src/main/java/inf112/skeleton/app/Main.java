package inf112.skeleton.app;

import javax.swing.JFrame;

import inf112.skeleton.app.controller.PlatformerController;
import inf112.skeleton.app.model.PlatformerModel;
import inf112.skeleton.app.model.level.PlatformerLevelFactory;
import inf112.skeleton.app.view.PlatformerView;
import inf112.skeleton.app.view.LevelThemes.DefaultLevelTheme;
import inf112.skeleton.eventBus.EventBus;

public class Main {
    public static void main(String[] args) {
        EventBus eventBus = new EventBus();
        PlatformerModel model = new PlatformerModel(eventBus, new PlatformerLevelFactory());
        PlatformerView view = new PlatformerView(model, eventBus, new DefaultLevelTheme("default"));
        new PlatformerController(model, view, eventBus);

        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("DungeonRun");
        frame.setContentPane(view);
        frame.pack();
        frame.setVisible(true);
    }
}