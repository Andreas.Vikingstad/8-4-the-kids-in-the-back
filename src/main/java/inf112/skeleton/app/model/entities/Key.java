package inf112.skeleton.app.model.entities;

import java.awt.Rectangle;

public class Key extends Entity{

  /**
   * Creates a new Key object with a given hitbox.
   * @param hitbox the hitbox of the Key.
   */
  public Key(Rectangle hitbox) {
    super(hitbox);
    this.c = 'K';
  }
}
