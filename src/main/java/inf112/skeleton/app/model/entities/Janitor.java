package inf112.skeleton.app.model.entities;

import java.awt.Rectangle;

import inf112.skeleton.app.model.Force;

public class Janitor extends Enemy{

  /**
   * Creates a new Janitor object with a given position and force.
   * @param hitbox the hitbox of the Janitor
   * @param force the force of the Janitor
   */
  public Janitor(Rectangle hitbox, Force force) {
    super(hitbox, force);
    this.c = 'J';
    this.health = 1;
    this.damage = 1;
  }
  
}
