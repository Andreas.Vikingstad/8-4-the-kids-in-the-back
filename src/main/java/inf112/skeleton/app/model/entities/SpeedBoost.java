package inf112.skeleton.app.model.entities;

import java.awt.Rectangle;

public class SpeedBoost extends Entity{

  /**
   * Creates a new SpeedBoost object with a given hitbox.
   * @param hitbox the hitbox of the SpeedBoost.
   */
  public SpeedBoost(Rectangle hitbox) {
    super(hitbox);
    this.c = 'B';
    this.damage = 0;
  }
  
}
