package inf112.skeleton.app.model.entities;

import java.awt.Rectangle;

import inf112.skeleton.app.model.Force;

public class Guard extends Enemy {

  /**
   * Creates a new Guard object with a given position and force.
   * @param hitbox the hitbox of the Guard
   * @param force the force of the Guard
   */
  public Guard(Rectangle hitbox, Force force) {
    super(hitbox, force);
    this.c = 'G';
    this.health = 1;
    this.damage = 1;
  }
  
}
