package inf112.skeleton.app.model.entities;

import java.awt.Rectangle;

public class Decorations extends Entity{

  private String type;

  /**
   * Creates a new Decoration object with a given rectangle and type.
   * 
   * @param hitbox the hitbox of the Decoration
   * @param type the type of the decoration
   */
  public Decorations(Rectangle hitbox, String type) {
    super(hitbox);
    this.c = 'F';
    this.type = type;
  }

  public String getType() {
    return type;
  }
}
