package inf112.skeleton.app.model.entities;

import java.awt.Rectangle;

import inf112.skeleton.app.model.Force;

public class Enemy extends ControllableEntity{

  /**
   * Creates a new Enemy object with a given position and force.
   * @param hitbox the hitbox of the Enemy
   * @param force the force of the Enemy
   */
  public Enemy(Rectangle hitbox, Force force) {
    super(hitbox, force);
    this.c = 'E';
    this.health = 1;
    this.damage = 1;
  }
}
  

