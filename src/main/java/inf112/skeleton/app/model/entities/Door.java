package inf112.skeleton.app.model.entities;

import java.awt.Rectangle;

public class Door extends Entity{

  private boolean locked;

  /**
   * Creates a new Door object with a given position and size.
   * @param hitbox the hitbox of the Door
   */
  public Door(Rectangle hitbox, Boolean locked) {
    super(hitbox);
    this.c = 'D';
    this.locked = locked;
  }

  public boolean isLocked() {
    return locked;
  }

  public void setLocked(boolean locked) {
    this.locked = locked;
  }
}
