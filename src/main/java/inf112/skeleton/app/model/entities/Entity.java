package inf112.skeleton.app.model.entities;

import java.awt.Rectangle;

import inf112.skeleton.app.model.GameObject;

public class Entity extends GameObject {

  protected int health;
  protected int damage;

  /**
   * Creates a new Entity object with a given hitbox
   * @param hitbox the hitbox of the Entity
   */
  public Entity(Rectangle hitbox) {
    super(hitbox);
  }

  /**
   * Gets the health of the entity
   * @return the health of the entity
   */
  public int getHealth() {
    if (this.health == '\u0000') {
      return -1;
    } else {
      return this.health;
    }
  }

  /**
   * Sets the health of the entity
   * @param health the health to set
   */
  public void setHealth(int health) {
    this.health = health;
  }

  /**
   * Gets the damage of the entity
   * @return the damage of the entity
   */
  public int getDamage() {
    if (this.damage == '\u0000') {
      return 0;
    } else {
      return this.damage;
    }
  }

 
}
