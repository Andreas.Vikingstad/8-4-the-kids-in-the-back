package inf112.skeleton.app.model.entities;

import java.awt.Rectangle;

public class Computer extends Entity{

  /**
   * Creates a new Computer object with a given hitbox.
   * @param hitbox the hitbox of the Computer.
   */
  public Computer(Rectangle hitbox) {
    super(hitbox);
    this.c = 'C';
  }
  
}
