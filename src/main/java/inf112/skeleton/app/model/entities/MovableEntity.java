package inf112.skeleton.app.model.entities;

import java.awt.Rectangle;

import inf112.skeleton.app.model.Force;

public class MovableEntity extends Entity{

  private Force force;
  private boolean isOnGround;

  /**
   * Creates a new MovableEntity object with a given hitbox and force.
   * @param hitbox the hitbox of the entity
   * @param force the force of the entity
   */
  public MovableEntity(Rectangle hitbox, Force force) {
    super(hitbox);
    this.force = force;
    this.isOnGround = false;
  }

  /**
   * Returns the force of the entity
   * @return the force of the entity
   */
  public Force getForce() {
      return force;
  }

  /**
   * Sets the force of the entity
   * @param force the force to set
   */
  public void setForce(Force force) {
    this.force = force;
  }

  /**
   * Returns a new rectangle that is shifted by deltaX and deltaY
   * @param deltaX the amount to shift the rectangle in the x direction
   * @param deltaY the amount to shift the rectangle in the y direction
   * @return a new rectangle that is shifted by deltaX and deltaY
   */
  public Rectangle shiftedBy(int deltaX, int deltaY) {
    int newX = this.hitbox.x + deltaX;
    int newY = this.hitbox.y + deltaY;
    return new Rectangle(newX, newY, this.hitbox.width, this.hitbox.height);
  }

  /**
   * Moves the entity by deltaX and deltaY
   * @param deltaX
   * @param deltaY
   */
  public void move(int deltaX, int deltaY) {
    this.hitbox.translate(deltaX, deltaY);
  }

  /**
   * Checks if the entity is on the ground
   * @return true if the entity is on the ground, false if not
   */
  public boolean isOnGround() {
    return isOnGround;
  }

  /**
   * Sets the entity to be on the ground or not
   * @param isOnGround true if the entity is on the ground, false if not
   */
  public void setOnGround(boolean isOnGround) {
    this.isOnGround = isOnGround;
  }
}
