package inf112.skeleton.app.model.entities;

import java.awt.Rectangle;

import inf112.skeleton.app.model.Force;

public class Player extends ControllableEntity {
  public static int lives = 3;
  
  /**
   * Creates a new Player object with a given position and force.
   * @param hitbox the hitbox of the Player
   * @param force the force of the Player
   */
  public Player(Rectangle hitbox, Force force) {
    super(hitbox, force);
    this.c = 'P';
    this.damage = 1;
  }

  /**
   * Gets the maximum health the player can have
   * @return the maximum health of the player
   */
  public int maxHealth() {
    return 3;
  }

  /**
   * Gets the current health of the player
   * @return the current health of the player
   */
  public int getHealth() {
    return Player.lives;
  }

  /**
   * Alteres the health of the player by a given amount
   * @param damage
   */
  public void takeDamage(int damage) {
    Player.lives -= damage;
  }
}