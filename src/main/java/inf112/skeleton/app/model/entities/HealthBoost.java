package inf112.skeleton.app.model.entities;

import java.awt.Rectangle;

public class HealthBoost extends Entity{

  /**
   * Creates a new HealthBoost object with a given hitbox.
   * @param hitbox the hitbox of the HealthBoost
   */
  public HealthBoost(Rectangle hitbox) {
    super(hitbox);
    this.c = 'H';
    this.damage = -1;
  }
    
}
