package inf112.skeleton.app.model.entities;

import java.awt.Rectangle;

import inf112.skeleton.app.model.Force;

public class ControllableEntity extends MovableEntity{

  private int moving;
  private int onWall;
  private boolean phasing;

  /**
   * Creates a new ControllableEntity object with a given hitbox and force.
   * @param hitbox the hitbox of the entity
   * @param force the force of the entity
   */
  public ControllableEntity(Rectangle hitbox, Force force) {
    super(hitbox, force);
    this.moving = 0;
    this.onWall = 0;
  }

  /**
   * Move the entity left.
   * @param move true if the entity should move left, false if not
   * @return true is the entity is supposed to move left, false if not
   */
  public boolean moveLeft(boolean move) {
    if (move) {
      changeMoving(-1);
      return true;
    }
    this.changeMoving(1);
    return false;
  }

  /**
   * Move the entity right.
   * @param move true if the entity should move right, false if not
   * @return true is the entity is supposed to move right, false if not
   */
  public boolean moveRight(boolean move) {
    if (move) {
      this.changeMoving(1);
      return true;
    }
    this.changeMoving(-1);
    return false;
  }

  /**
   * Make the entity jump.
   * @return true if the entity is able to jump, false if not
   */
  public boolean jump() {
    if (!this.isOnGround() && this.isOnWall() == 0) return false;
    this.setPhasing(true);
    if (this.isOnWall() == 1) {
      this.setForce(this.getForce().setXForce(-7));;
      this.setForce(this.getForce().setYForce(-10));
      return true;
    }
    if (this.isOnWall() == -1) {
      this.setForce(this.getForce().setXForce(7));
      this.setForce(this.getForce().setYForce(-10));
      return true;
    }
    this.setForce(this.getForce().setYForce(-10));
    return true;
  }

  /**
   * Adjust the speed of the entity.
   * If the entity is not moving, the speed is adjusted towards 0.
   * If the entity is moving, the speed is adjusted towards 5 or -5.
   * depending on the direction the entity is moving.
   */
  public void adjustSpeed() {
    if (this.moving == 0) {
      if (this.isOnGround()) {
        this.setForce(this.getForce().setXForce(0));
      } else {
        if (this.getForce().xForce() > 0) {
          this.setForce(this.getForce().changeXForce(-1));
        }
        if (this.getForce().xForce() < 0) {
          this.setForce(this.getForce().changeXForce(1));
        }
      }
    }
    if (this.moving == 1 ) {
      if (this.isOnGround()) {
        this.setForce(this.getForce().setXForce(5));
      } else {
        if (this.getForce().xForce() > 5) {
          this.setForce(this.getForce().changeXForce(-1));
        }
        if (this.getForce().xForce() < 5) {
          this.setForce(this.getForce().changeXForce(1));
        }
      }
    }
    if (this.moving == -1) {
      if (this.isOnGround()) {
        this.setForce(this.getForce().setXForce(-5));
      } else {
        if (this.getForce().xForce() > -5) {
          this.setForce(this.getForce().changeXForce(-1));
        }
        if (this.getForce().xForce() < -5) {
          this.setForce(this.getForce().changeXForce(1));
        }
      }
    }
  }

  /**
   * Get the moving variable of the entity.
   * @return -1 if moving left, 0 if not moving, 1 if moving right
   */
  public int getMoving() {
    return moving;
  }

  /**
   * Set the moving variable of the entity.
   * @param moving -1 if moving left, 0 if not moving, 1 if moving right
   */
  public void changeMoving(int deltaMove) {
    if (deltaMove < -1) deltaMove = -1;
    if (deltaMove > 1) deltaMove = 1;
    if (this.moving + deltaMove > 1) this.moving = 1;
    else if (this.moving + deltaMove < -1) this.moving = -1;
    else this.moving += deltaMove;
  }

  /**
   * Get the onWall variable of the entity.
   * @return -1 if wall is left of entity, 0 if not on wall, 1 if wall is left of entity
   */
  public int isOnWall() {
    return onWall;
  }

  /**
   * Set the onWall variable of the Player.
   * @param onWall -1 if wall is left of entity, 0 if not on wall, 1 if wall is left of entity
   */
  public boolean setOnWall(int onWall) {
    if (onWall < -1) onWall = -1;
    if (onWall > 1) onWall = 1;
    this.onWall = onWall;
    return this.onWall != 0;
  }

  /**
   * Get the phasing variable of the entity.
   * @return true if entity is phasing, false if not
   */
  public boolean isPhasing() {
    return this.phasing;
  }

  /**
   * Set the phasing variable of the entity.
   * @param phasing true if entity is phasing, false if not
   * @return true if entity is phasing, false if not
   */
  public boolean setPhasing(boolean phasing) {
    this.phasing = phasing;
    return this.phasing;
  }
  
}
