package inf112.skeleton.app.model;

public enum GameState {
    WELCOME_SCREEN(0),
    LEVEL_ONE(1),
    LEVEL_TWO(2),
    LEVEL_THREE(3),
    LEVEL_FOUR(4),
    LEVEL_FIVE(5),
    LEVEL_SIX(6),
    GAME_OVER(0),
    PAUSED(0),
    VICTORY(0);

    public final int levelNumber;

    GameState(int levelNumber) {
      this.levelNumber = levelNumber;
    }
}
