package inf112.skeleton.app.model;

import java.awt.Point;
import java.awt.Rectangle;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import inf112.skeleton.app.controller.ControllablePlatformerModel;
import inf112.skeleton.app.model.entities.*;
import inf112.skeleton.app.model.level.*;
import inf112.skeleton.app.model.tiles.Tile;
import inf112.skeleton.app.view.ViewablePlatformerModel;
import inf112.skeleton.eventBus.EventBus;
import inf112.skeleton.eventBus.RepaintEvent;
import inf112.skeleton.eventBus.SoundEvent;

public class PlatformerModel implements ViewablePlatformerModel, ControllablePlatformerModel {
  
  private EventBus eventBus;
  private PlatformerLevel level;
  private PlatformerLevelFactory factory;
  private Player player = null;
  private GameState state;
  private ArrayList<Entity> entities;
  private Timer timer;
  private int characterSkin;
  private boolean canTakeDamage = true;
  private float damageCooldown = 1.0f;
  private float damageTimer = 0.0f;
  private boolean canPlaySound = true;
  private float soundCooldown = 1.0f;
  private float soundTimer = 0.0f;
  private boolean hasKey = false;
  private boolean speedBoost = false;

  public PlatformerModel(EventBus eventBus, PlatformerLevelFactory factory) {
    this.eventBus = eventBus;
    this.factory = factory;
    this.level = factory.createLevel(getStringFromFile("level1.txt"));
    this.state = GameState.WELCOME_SCREEN;
    characterSkin = 1;
  }

  // Methods for changing levels

  private void changeLevel() {
    switch (this.state) {
      case WELCOME_SCREEN: {
        initializeLevel(level);
        startTimer();
        this.state = GameState.LEVEL_ONE;
        break;
      }
      case GAME_OVER: break;
      case LEVEL_ONE: {
        level = factory.createLevel(getStringFromFile("level2.txt"));
        initializeLevel(level);
        this.state = GameState.LEVEL_TWO;
        break;
      }
      case LEVEL_TWO: {
        level = factory.createLevel(getStringFromFile("level3.txt"));
        initializeLevel(level);
        this.state = GameState.LEVEL_THREE;
        break;
      }
      case LEVEL_THREE: {
        level = factory.createLevel(getStringFromFile("level4.txt"));
        initializeLevel(level);
        this.state = GameState.LEVEL_FOUR;
        break;
        }
      case LEVEL_FOUR: {
        level = factory.createLevel(getStringFromFile("level5.txt"));
        initializeLevel(level);
        this.state = GameState.LEVEL_FIVE;
        break;
      }
      case LEVEL_FIVE: {
        level = factory.createLevel(getStringFromFile("level6.txt"));
        initializeLevel(level);
        this.state = GameState.LEVEL_SIX;
        break;
      }
      case LEVEL_SIX: {
        state = GameState.VICTORY;
        break;
      }
      case VICTORY: break;
      default:
        throw new IllegalArgumentException("Invalid game state: " + state);
    }
  }
  

  /**
   * Initializes the level by creating a player and setting the entities
   * @param level the level to initialize
   */
  private void initializeLevel(PlatformerLevel level) {
    Point startPos = level.getStarPosition();
    this.entities = level.getEntities();
    Force force = new Force(0, 0);
    this.hasKey = false;
    this.speedBoost = false;
    if (player != null) {
      force = player.getForce();
      int playerMoving = player.getMoving();
      this.player = new Player(new Rectangle(startPos.x, startPos.y, 20, 20), force);
      this.player.changeMoving(playerMoving);
    } else this.player = new Player(new Rectangle(startPos.x, startPos.y, 20, 20), force);
  }

  private void startTimer() {
    timer = new Timer();
    int ticksPerSecond = 30;
    TimerTask runGame = new TimerTask() {
      @Override
      public void run() {
        doTick();
      }
    };
    timer.schedule(runGame, 0, 1000/ticksPerSecond);
  }

  /**
   * Reads a file and returns the lines as a list of strings
   * @param filename the name of the file to read
   * @return the lines of the file as a list of strings
   */
  private static List<String> getStringFromFile(String filename) {
    List<String> lines = new ArrayList<>();
    BufferedReader reader = new BufferedReader(new InputStreamReader(
    Objects.requireNonNull(PlatformerLevelFactory.class.getResourceAsStream(filename)),
    StandardCharsets.UTF_8
    ));
    reader.lines().forEach(lines::add);
    lines.replaceAll(String::strip);
    lines.removeIf(String::isEmpty);
    return lines;
  }



  // Methods for game logic

  /**
   * Does a tick of the game
   */
  private void doTick() {
    for (Entity entity : entities) {
      if (entity instanceof MovableEntity) doForce((MovableEntity)entity);
      if (entity instanceof Door && ((Door)entity).isLocked() == true && hasKey == true) {
        ((Door)entity).setLocked(false);
      }
    }
    doForce(player);
    checkPlayerOverlap();
    if (!canTakeDamage) {
      damageTimer += 1.0f/20.0f;
      if (damageTimer >= damageCooldown) {
        canTakeDamage = true;
        damageTimer = 0.0f;
      }
    }
    if (!canPlaySound) {
      soundTimer += 1.0f/60.0f;
      if (soundTimer >= soundCooldown) {
        canPlaySound = true;
        soundTimer = 0.0f;
      }
    }
    eventBus.post(new RepaintEvent());
  }

  /**
   * Returns the tiles that are surrounding the entity
   * @param entity the entity to check
   * @return the tiles that are surrounding the entity
   */
  private ArrayList<Tile> getSurroundingTiles(MovableEntity entity) {
    ArrayList<Tile> tiles = new ArrayList<Tile>();
    Rectangle checkBox = new Rectangle(entity.getHitbox());
    checkBox.grow(1, 1);
    for (Tile tile : level.getTiles()) {
      if (tile.getHitbox().intersects(checkBox)) {
        tiles.add(tile);
      }
    }
    return tiles;
  }

  /**
   * Checks if the entity is on the ground
   * and sets the onGround variable of the entity
   * @param entity the entity to check
   * @return true if the entity is on the ground, false if not
   */
  private boolean isOnGround(MovableEntity entity) {
    ArrayList<Tile> tiles = getSurroundingTiles(entity);
    for (Tile tile : tiles) {
      if (tile.isSolid()) {
        Rectangle upShiftedTile = new Rectangle(tile.getHitbox());
        upShiftedTile.translate(0, -1);
        if (tile.isPhaseable() && entity instanceof ControllableEntity) {
          if (((ControllableEntity)entity).isPhasing()) {
            continue;
          }
        }
        if (entity.getHitbox().intersects(upShiftedTile)) {
          entity.setOnGround(true);
          return true;
        }
      }
    }
    entity.setOnGround(false);
    return false;
  }

  /**
   * Checks if the entity is on a wall
   * and sets the onWall variable of the entity
   * @param entity the entity to check
   * @return true if the entity is on a wall, false if not
   */
  private boolean isOnWall(ControllableEntity entity) {
    ArrayList<Tile> tiles = getSurroundingTiles(entity);
    if (!entity.isOnGround()) {
      for (Tile tile : tiles) {
        if (tile.isSolid() && !tile.isPhaseable()) {
          Rectangle wideTileHitbox = new Rectangle(tile.getHitbox());
          wideTileHitbox.grow(1, 0);
          if (entity.getHitbox().intersects(wideTileHitbox)) {
            if (entity.getHitbox().x < tile.getHitbox().x) {
              entity.setOnWall(1);
            } else {
              entity.setOnWall(-1);
            }
            return true;
          }
        }
      }
    }
    entity.setOnWall(0);
    return false;
  }
  
  /**
   * Checks if an entity should be phasing
   * and sets the phasing variable of the entity
   * @param entity the entity to check
   */
  private boolean isPhasing(ControllableEntity entity) {
    if (entity.getForce().yForce() < 0) { //if the entity is moving up it is phasing
      entity.setPhasing(true);
      return true;
    }
    //if the entity is inside a phaseable tile, it is also phasing
    ArrayList<Tile> tiles = getSurroundingTiles(entity);
    for (Tile tile : tiles) {
      if (tile.isPhaseable()) {
        if (entity.getHitbox().intersects(tile.getHitbox())) {
          entity.setPhasing(true);
          return true;
        }
      }
    }
    entity.setPhasing(false);
    return false;
  }

  /**
   * Does force calculations for an entity
   * @param entity the entity to do force calculations for
   */
  private void doForce(MovableEntity entity) {
    isOnGround(entity);
    if (entity instanceof ControllableEntity) {
      isOnWall((ControllableEntity)entity);
      ((ControllableEntity)entity).adjustSpeed();
    }
    Force force = entity.getForce();
    int xForce = force.xForce();
    if (entity instanceof Player && (player.isOnWall() == 0) && speedBoost) {
      xForce = xForce * 2;
    }
    int yForce = force.yForce();
    
    //try to move entity
    if (!moveEntity(entity, xForce, yForce)) {
      //if entity could not move, check if it is colliding with a solid tile
      getSurroundingTiles(entity).forEach(tile -> {
        //if the tile is solid
        if (tile.isSolid()) {
          Rectangle yTile = new Rectangle(tile.getHitbox());
          yTile.grow(0, 1);
          if (entity.getHitbox().intersects(yTile)) { //if entity is below or above tile
            if (tile.isPhaseable() && entity instanceof ControllableEntity) {
              if (((ControllableEntity)entity).isPhasing()) {
                return;
              }
            }
            entity.setForce(entity.getForce().setYForce(0));
          }
          Rectangle xTile = new Rectangle(tile.getHitbox());
          xTile.grow(1, 0);
          if (entity.getHitbox().intersects(xTile)) { //if entity is to the right or left of tile
            //if the entity is a player and is not moving, stop it from moving
            if (entity instanceof ControllableEntity) {
              if (((ControllableEntity) entity).getMoving() == 0) {
                entity.setForce(entity.getForce().setXForce(0));
              }
            } else {
              entity.setForce(entity.getForce().setXForce(0));
            }
          }
        }
      });
    }

    //apply gravity

    if (entity instanceof ControllableEntity) isPhasing((ControllableEntity)entity);

    if (!isOnGround(entity)) {
      if (entity instanceof ControllableEntity && isOnWall((ControllableEntity)entity)) {
        if (entity.getForce().yForce() < 2) {
          entity.setForce(entity.getForce().changeYForce(1));
        } else {
          entity.setForce(entity.getForce().setYForce(2));
        }
      } else {
        entity.setForce(entity.getForce().changeYForce(1));
      }
    } else {
      if (entity instanceof ControllableEntity && isPhasing((ControllableEntity) entity)) {
        entity.setForce(entity.getForce().changeYForce(1));
      }
    }
  }

  /**
   * Moves an entity by deltaX and deltaY
   * @param entity the entity to move
   * @param deltaX the amount to move the entity in the x direction
   * @param deltaY the amount to move the entity in the y direction
   * @return true if the entity was able to move, false if not
   */
  private boolean moveEntity(MovableEntity entity, int deltaX, int deltaY) {
    int xMod = deltaX >= 0 ? 1 : -1;
    int yMod = deltaY >= 0 ? 1 : -1;
    boolean moved = true;

    for (int x = 0; x < Math.abs(deltaX); x++) {
      if (isLegalMove(1 * xMod, 0, entity)) {
        entity.move(1 * xMod, 0);
      } else {
        moved = false;
      }
    }
    for (int y = 0; y < Math.abs(deltaY); y++) {
      if (isLegalMove(0, 1 * yMod, entity)) {
        entity.move(0, 1 * yMod);
      } else {
        moved = false;
      }
    }
    return moved;
  }

  /**
   * Checks if an entity can move by deltaX and deltaY
   * @param deltaX the amount to move the entity in the x direction
   * @param deltaY the amount to move the entity in the y direction
   * @param entity the entity to check
   * @return true if the entity can move, false if not
   */
  private boolean isLegalMove(int deltaX, int deltaY, MovableEntity entity) {
    Rectangle newHitbox = entity.shiftedBy(deltaX, deltaY);
    for (Tile tile : level.getTiles()) {
      if (tile.isPhaseable() && entity instanceof ControllableEntity) {
        if (((ControllableEntity)entity).isPhasing()) {
          continue;
        }
      }
      if (newHitbox.intersects(tile.getHitbox())) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if the player is overlapping with any entities
   */
  private void checkPlayerOverlap() {
    for (int i = 0; i < entities.size(); i++) {
      Entity entity = entities.get(i);
      if (entity.objectsOverlap(player)) {
        if (entity instanceof Guard && canTakeDamage) {
          eventBus.post(new SoundEvent("hurt2"));
          player.takeDamage(entity.getDamage());
          canTakeDamage = false;
          if (player.getHealth() <= 0) {
            eventBus.post(new SoundEvent("lost"));
            timer.cancel();
            state = GameState.GAME_OVER;
          }
        }
        if (entity instanceof Janitor && canTakeDamage) {
          eventBus.post(new SoundEvent("hurt1"));
          player.takeDamage(entity.getDamage());
          canTakeDamage = false;
          if (player.getHealth() <= 0) {
            eventBus.post(new SoundEvent("lost"));
            state = GameState.GAME_OVER;
          }
        }
        if (entity instanceof HealthBoost) {
          if (player.getHealth() < player.maxHealth()) {
            eventBus.post(new SoundEvent("healthBoost"));
            player.takeDamage(entity.getDamage());
            entities.remove(i);
          }
        }
        if (entity instanceof Door) {
          if(((Door)entity).isLocked() == false){
            eventBus.post(new SoundEvent("door"));
            changeLevel();
          }
          if(((Door)entity).isLocked() == true && hasKey == true){
            eventBus.post(new SoundEvent("door"));
            changeLevel();
          }
          if(((Door)entity).isLocked() == true && hasKey == false && canPlaySound == true){
            eventBus.post(new SoundEvent("locked"));
            canPlaySound = false;
          }
        }
        if (entity instanceof SpeedBoost) {
          speedBoost = true;
          entities.remove(i);
        }


        if (entity instanceof Computer) {
          changeLevel();
        }

        if (entity instanceof Key) {
          hasKey = true;
          entities.remove(i);
        }
      }
    }
  }

  // Interface methods

  // Used by both

  @Override
  public GameState getGameState() {
    return state;
  }

  // ViewablePlatformerModel methods

  @Override
  public Player getPlayer() {
    return player;
  }

  @Override
  public Point getDimension() {
    return level.getDimensions();
  }

  @Override
  public ArrayList<Entity> getEntities() {
    return entities;
  }

  @Override
  public ArrayList<Tile> getTiles() {
    return level.getTiles();
  }

  @Override
  public int getPlayerSpriteInt() {
    return characterSkin;
  }

  // ControllablePlatformerModel methods

  @Override
  public void startGame() {
    changeLevel();
  }

  private GameState prevState;

  @Override
  public void pauseGame() {
    prevState = getGameState();
    state = GameState.PAUSED;
    timer.cancel();
  }

  @Override
  public void unPause() {
    state = prevState;
    startTimer();
  }

  @Override
  public void restartGame() {
    state = GameState.WELCOME_SCREEN;
    Player.lives = 3;
    timer.cancel();
    level = factory.createLevel(getStringFromFile("level1.txt"));
    initializeLevel(level);
    startGame();
    
  }

  @Override
  public void changeSkin(int i) {
    characterSkin = i;
  }

  @Override
  public boolean movePlayerLeft(boolean moveLeft) {
    return player.moveLeft(moveLeft);
  }

  @Override
  public boolean movePlayerRight(boolean moveRight) {
    return player.moveRight(moveRight);
  }

  @Override
  public boolean jumpPlayer() {
    return player.jump();
  }

  @Override
  public boolean movePlayerDown() {
    if (player.isOnGround()) player.setForce(player.getForce().setYForce(1));
    player.setPhasing(true);
    return true;
  }
}
