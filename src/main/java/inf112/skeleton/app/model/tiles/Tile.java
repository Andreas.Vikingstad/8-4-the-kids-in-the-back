package inf112.skeleton.app.model.tiles;

import java.awt.Rectangle;

import inf112.skeleton.app.model.GameObject;

public class Tile extends GameObject {

  protected boolean isSolid;
  protected boolean isPhaseable;
  
  /**
   * creates a new Tile object with a given hitbox.
   * @param hitbox the hitbox of the Tile
   */
  public Tile(Rectangle hitbox) {
    super(hitbox);
  }

  /**
   * checks if the Tile is solid
   * @return true if the Tile is solid, false otherwise
   */
  public boolean isSolid() {
    return isSolid;
  }

  /**
   * checks if the Tile is phaseable
   * @return true if the Tile is phaseable, false otherwise
   */
  public boolean isPhaseable() {
    return isPhaseable;
  }

}
