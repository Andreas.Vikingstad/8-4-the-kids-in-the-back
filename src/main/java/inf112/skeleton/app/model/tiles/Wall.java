package inf112.skeleton.app.model.tiles;

import java.awt.Rectangle;

public class Wall extends Tile{

  /**
   * Creates a new Wall object with a given hitbox.
   * @param hitbox the hitbox of the Wall
   */
  public Wall(Rectangle hitbox) {
    super(hitbox);
    this.c = '#';
    this.isSolid = true;
    this.isPhaseable = false;
  }
  
}
