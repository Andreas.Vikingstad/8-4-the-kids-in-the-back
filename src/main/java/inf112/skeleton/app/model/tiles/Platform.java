package inf112.skeleton.app.model.tiles;

import java.awt.Rectangle;

public class Platform extends Tile {

  /**
   * Creates a new Platform object with a given hitbox.
   * @param hitbox the hitbox of the Platform
   */
  public Platform(Rectangle hitbox) {
    super(hitbox);
    this.c = '_';
    this.isSolid = true;
    this.isPhaseable = true;
  }
}
