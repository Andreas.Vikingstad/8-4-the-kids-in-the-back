package inf112.skeleton.app.model;

import java.awt.Rectangle;

public class GameObject {

  protected Rectangle hitbox;
  protected char c;

  /**
   * creates a new GameObject with a given hitbox
   * @param hitbox the hitbox of the GameObject
   */
  public GameObject(Rectangle hitbox) {
    this.hitbox = hitbox;
  }

  /**
   * Checks if two GameObjects overlap by comparing their grid cells
   * @param object The other GameObject
   * @return True if the GameObjects overlap, false otherwise
   */
  public boolean objectsOverlap(GameObject object) {
    return this.getHitbox().intersects(object.getHitbox());
  }

  /**
   * Gets the hitbox of the GameObject
   * @return the hitbox of the GameObject
   */
  public Rectangle getHitbox() {
    return this.hitbox;
  }

  /**
   * Gets the character representation of the GameObject
   * @return the character representation of the GameObject
   */
  public char getChar() {
    if (this.c == '\u0000') {
      throw new NullPointerException("Character not set");
    } else {
      return this.c;
    }
  }

}
