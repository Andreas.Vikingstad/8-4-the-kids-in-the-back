package inf112.skeleton.app.model;

/**
 * A force power that affects movement of an entity.
 * 
 * @param xForce the movement effect on the x-axis
 * @param yForce the movement effect on the y-axis
 */
public record Force(int xForce, int yForce) {

  @Override
  public final boolean equals(Object obj) {
    if (obj == null) return false;
    if (obj == this) return true;
    if (!(obj instanceof Force)) return false;
    Force force = (Force) obj;
    return this.xForce == force.xForce && this.yForce == force.yForce;
  }

  @Override
  public final int hashCode() {
    return xForce + yForce;
  }

  @Override
  public final String toString() {
    return "Force: xForce = " + xForce + ", yForce = " + yForce;
  }

  /**
   * Returns the xForce of the Force
   * @return the xForce of the Force
   */
  public final int xForce() {
    return xForce;
  }

  /**
   * Returns the yForce of the Force
   * @return the yForce of the Force
   */
  public final int yForce() {
    return yForce;
  }

  /**
   * Sets the xForce of the Force
   * @param deltaX the new xForce
   * @param deltaY the new yForce
   * @return a new Force with the new xForce and yForce
   */
  public final Force changeForce(int deltaX, int deltaY) {
    return new Force(xForce + deltaX, yForce + deltaY);
  }

  /**
   * Sets the xForce of the Force
   * @param newForce the new xForce
   * @return a new Force with the new xForce
   */
  public final Force setXForce(int newForce) {
    return new Force(newForce, yForce);
  }

  /**
   * Sets the yForce of the Force
   * @param newForce the new yForce
   * @return a new Force with the new yForce
   */
  public final Force setYForce(int newForce) {
    return new Force(xForce, newForce);
  }

  /**
   * Changes the xForce of the Force by deltaX
   * @param deltaX the amount to change the xForce by
   * @return a new Force with the new xForce
   */
  public final Force changeXForce(int deltaX) {
    return new Force(xForce + deltaX, yForce);
  }

  /**
   * Changes the yForce of the Force by deltaY
   * @param deltaY the amount to change the yForce by
   * @return a new Force with the new yForce
   */
  public final Force changeYForce(int deltaY) {
    return new Force(xForce, yForce + deltaY);
  }

}
