package inf112.skeleton.app.model.level;

import java.awt.Point;
import java.util.ArrayList;

import inf112.skeleton.app.model.entities.Entity;
import inf112.skeleton.app.model.tiles.Tile;

public class PlatformerLevel{
    
    ArrayList<Entity> entities;
    Point starPosition;
    ArrayList<Tile> tiles;
  
    /**
     * Creates a new PlatformerLevel object with a given list of tiles, start position, end position and list of enemies.
     * @param tiles the list of tiles
     * @param startPos the start position
     * @param entities the list of entities
     */
    public PlatformerLevel(ArrayList<Tile> tiles, Point startPos, ArrayList<Entity> entities) {
        this.starPosition = startPos;
        this.tiles = tiles;
        this.entities = entities;
    }

    /**
     * Gets the player startposition for current level
     * @return startposition for current level
     */
    public Point getStarPosition() {
        return starPosition;
    }

    /**
     * Gets the entities for current level
     * @return entities for current level
     */
    public ArrayList<Tile> getTiles() {
        return tiles;
    }

    /**
     * Gets the entities for current level
     * @return entities for current level
     */
    public ArrayList<Entity> getEntities() {
        return entities;
    }

    /**
     * gets the dimensions of the level
     * @return the dimensions of the level as a Position
     */
    public Point getDimensions() {
        int largestX = 0;
        int largestY = 0;
        int size = 0;
        for (Tile tile : tiles) {
            Point pos = tile.getHitbox().getLocation();
            if (pos.x > largestX) {
                largestX = pos.x;
            }
            if (pos.y > largestY) {
                largestY = pos.y;
            }
            size = (int) tile.getHitbox().getWidth();
        }
        return new Point(largestX + size, largestY + size);
    }
  
  }

   