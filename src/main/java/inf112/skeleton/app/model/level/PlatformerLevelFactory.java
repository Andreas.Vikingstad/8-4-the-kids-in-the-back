package inf112.skeleton.app.model.level;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import inf112.skeleton.app.model.Force;
import inf112.skeleton.app.model.entities.*;
import inf112.skeleton.app.model.tiles.*;

public class PlatformerLevelFactory {
    
  /**
   * Creates a level from a string array
   * @param level the string array
   * @return the level
   */
  public PlatformerLevel createLevel(List<String> level) {
    Point startPos = null;
    ArrayList<Entity> entities = new ArrayList<>();
    ArrayList<Tile> tiles = new ArrayList<>();
    for (int y = 0; y < level.size(); y++) {
      for (int x = 0; x < level.get(y).length(); x++){
        Point pos = new Point(x*20, y*20);
        Tile tile;
        switch (level.get(y).charAt(x)) {
          case 'S':
            startPos = pos;
            break;
          case 'D':
            entities.add(new Door(new Rectangle(x*20, y*20, 20, 20), false));
            break;
          case 'G':
            entities.add(new Guard(new Rectangle(x*20, y*20, 20, 20), new Force(0, 0)));
            break;
          case 'J':
            entities.add(new Janitor(new Rectangle(x*20, y*20, 20, 20), new Force(0, 0)));
            break;
          case 'H':
            int energyOffset = 5;
            entities.add(new HealthBoost(new Rectangle(x*20, y*20, 20, 20 + energyOffset)));
            break;
          case 'B':
            entities.add(new SpeedBoost(new Rectangle(x*20, y*20, 20, 20)));
            break;
          case 'K':
            entities.add(new Key(new Rectangle(x*20, y*20, 20, 20)));
            break;
          case 'C':
            entities.add(new Computer(new Rectangle(x*20, y*20, 20, 20)));
            break;
          case 'L':
            entities.add(new Door(new Rectangle(x*20, y*20, 20, 20), true));
            break;
          case 'Q':
            entities.add(new Decorations(new Rectangle(x*20, y*20, 20, 20), "table"));
            break;
          case 'V':
            entities.add(new Decorations(new Rectangle(x*20, y*20, 20, 20), "green_chair"));
            break;
          case 'R':
            entities.add(new Decorations(new Rectangle(x*20, y*20, 20, 20), "locker"));
            break;
          case 'W':
            entities.add(new Decorations(new Rectangle(x*20, y*20, 20, 20), "blue_chair"));
            break;
          case '#':
            tile = new Wall(new Rectangle(x*20, y*20, 20, 20));
            tiles.add(tile);
            break;
          case '_':
            tile = new Platform(new Rectangle(x*20, y*20, 20, 20));
            tiles.add(tile);
            break;
          case '-':
            break;
          default:
            break;
        }
      }
    }
    if (startPos == null) throw new IllegalArgumentException("No start position in level string");
    return new PlatformerLevel(tiles, startPos, entities);      
  }
    
  /**
   * Creates a level from a string
   * @param level the string
   * @return the level as a PlatformerLevel
   */
  public PlatformerLevel createLevelFromString(String level) {
    String[] lines = level.split("\n");
    List<String> levelList = new ArrayList<>();
    for (String line : lines) {
      levelList.add(line);
    }
    return createLevel(levelList);
  }
}
