# Rapport – innlevering 4
**8-4:** *The Kids in The Back* – *Erik, Snorre , Nicolai, Andreas, Elisabeth*

Hvordan fungerer rollene i teamet? Trenger dere å oppdatere hvem som er teamlead eller kundekontakt?


*Rollene fungerer bra, men vi har ikke hatt noen tydelige roller mer en helhetlig samarbeids fase, vi har beholdt rollene likt gjennom alt så lang*


Trenger dere andre roller? Skriv ned noen linjer om hva de ulike rollene faktisk innebærer for dere.


*Vi la inn rolle for lyd og lagde ett google skjema som beskrev hvilke oppgaver og hvem som utførte dem dette ga oss en bedre oversikt over hva som ikke hadde blitt gjort enda og hjalp oss å fordele oppgaver*


*Rollene for oss representerer hvem som skal ha hoved oversikten for ett visst område, men ikke ha helhetlig ansvar for dette*


Er det noen erfaringer enten team-messig eller mtp prosjektmetodikk som er verdt å nevne? Synes teamet at de valgene dere har tatt er gode? Hvis ikke, hva kan dere gjøre annerledes for å forbedre måten teamet fungerer på?


*Vi har tatt gode valg når det kommer til kommuniksjon aller er med på å møtes og snakke om arbeidet og hva som skal gjøres, det har vært god kommunikasjon og har ikke noe vi ønsker å forrandre på teamet så langt*


Hvordan er gruppedynamikken? Er det uenigheter som bør løses?


*Det har vært god dynamikk, litt uenigheter her og der, men ikke noe vi ikke har snakket ut om å funnet til enighet i*


Hvordan fungerer kommunikasjonen for dere?


*veldig bra*


Gjør et kort retrospektiv hvor dere vurderer hva dere har klart til nå, og hva som kan forbedres. Dette skal handle om prosjektstruktur, ikke kode. Dere kan selvsagt diskutere kode, men dette handler ikke om feilretting, men om hvordan man jobber og kommuniserer.


*syns Prosjekter virkelig kommer sammen nå og at vi nærmer oss ett fullført produkt som kan vises*


Under vurdering vil det vektlegges at alle bidrar til kodebasen. Hvis det er stor forskjell i hvem som committer, må dere legge ved en kort forklaring for hvorfor det er sånn. Husk å committe alt. (Også designfiler)


*Vi har jobber sammen og sitter ofte og par programerer slik at det er mye samarbeid som ikke syns, men vi er fornøyd med det alle har bidrat til kodebasen så langt*


Referat fra møter siden forrige leveranse skal legges ved (mange av punktene over er typisk ting som havner i referat).
Bli enige om maks tre forbedringspunkter fra retrospektivet, som skal følges opp under neste sprint.


*Vi har lagt ved logg for hvert møte i Discord, dette for at alle skal ha rask og enkelt tilgang til de både på telefon og på pc, vi lager en mappe ved slutten av prosjektet som inneholder alle loggene med dato, som vi ønsket å gjøre som ett team.*
