# Rapport – innlevering 2
**8-4:** *The Kids in The Back* – *Erik, Andreas, Nicolai, Snorre, Elisabeth,*

Hvordan fungerer rollene i teamet? Trenger dere å oppdatere hvem som er teamlead eller kundekontakt?
*Rollene fungerer bra, men vi har ikke hatt noen tydelige roller mer en helhetlig samarbeids fase, vi har beholdt rollene likt gjennom alt så lang*
Trenger dere andre roller? Skriv ned noen linjer om hva de ulike rollene faktisk innebærer for dere.
*Vi har så langt ikke trengt noen flere roller en det vi hadde planlagt fra start, men tror at vi må det i fremtiden*
*Rollene for oss representerer hvem som skal ha hoved oversikten for ett visst område, men ikke ha helhetlig ansvar for dette*
Er det noen erfaringer enten team-messig eller mtp prosjektmetodikk som er verdt å nevne? Synes teamet at de valgene dere har tatt er gode? Hvis ikke, hva kan dere gjøre annerledes for å forbedre måten teamet fungerer på?
*vi har så langt møttes ofte og snakket ut om eventuelle beskymringer og spørsmål vi har for hverandres kode, vi har vært flinke til å gi beskjed og jobbe i branches. Tror det hjelper at vi kjenner hverandre fra før av og har god dynamikk, samt ikke er redd for å spør spørsmål*
Hvordan er gruppedynamikken? Er det uenigheter som bør løses?
*foreløpig ikke uenigheter, vi snakker ut og diskuterer forskjellig løsninger*
Hvordan fungerer kommunikasjonen for dere?
*veldig bra*
Gjør et kort retrospektiv hvor dere vurderer hva dere har klart til nå, og hva som kan forbedres. Dette skal handle om prosjektstruktur, ikke kode. Dere kan selvsagt diskutere kode, men dette handler ikke om feilretting, men om hvordan man jobber og kommuniserer.
*syns vi har klart å bygge opp ett relativt greit prosjekt så langt, da vi har veldig forskjellig timeplaner har vi fremdeles klart å møte hverandre og jobbe kontinuerlig med prosjektet*
Under vurdering vil det vektlegges at alle bidrar til kodebasen. Hvis det er stor forskjell i hvem som committer, må dere legge ved en kort forklaring for hvorfor det er sånn. Husk å committe alt. (Også designfiler)
*Vi har jobber sammen og sitter ofte og par programerer slik at det er mye samarbeid som ikke syns, men vi er fornøyd med det alle har bidrat til kodebasen så langt*
Referat fra møter siden forrige leveranse skal legges ved (mange av punktene over er typisk ting som havner i referat).
Bli enige om maks tre forbedringspunkter fra retrospektivet, som skal følges opp under neste sprint.
*Vi har lagt ved logg for hvert møte i Discord, dette for at alle skal ha rask og enkelt tilgang til de både på telefon og på pc, vi lager en mappe ved slutten av prosjektet som inneholder alle loggene med dato, dette er noe vi i teamet har blitt enige om*