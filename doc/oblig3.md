# Rapport – innlevering 3
**8-4:** *The Kids in The Back<3* – *Erik, snorre, Nicolai, Andreas, Elisabeth*...


> Hvordan fungerer rollene i teamet? Trenger dere å oppdatere hvem som er teamlead eller kundekontakt?

*Rollene fungerer bra, men vi har ikke hatt noen tydelige roller mer en helhetlig samarbeids fase, Vi er fornøyd med fordelingen så langt*

> Trenger dere andre roller? Skriv ned noen linjer om hva de ulike rollene faktisk innebærer for dere.

*Vi har ikke måttet oppdatere rollene enda*

*Rollene for oss representerer hvem som skal ha hoved oversikten for ett visst område, men ikke ha helhetlig ansvar for dette*

> Er det noen erfaringer enten team-messig eller mtp prosjektmetodikk som er verdt å nevne? Synes teamet at de valgene dere har tatt er gode? Hvis ikke, hva kan dere gjøre annerledes for å forbedre måten teamet fungerer på?

*vi har så langt møttes ofte og snakket ut om eventuelle beskymringer og spørsmål vi har for hverandres kode, vi har vært flinke til å gi beskjed og jobbe i branches. Tror det hjelper at vi kjenner hverandre fra før av og har god dynamikk, samt ikke er redd for å spør spørsmål, dette er uendret fra sist*

> Hvordan er gruppedynamikken? Er det uenigheter som bør løses?

*Snakker ut om uenigheter og diskuterer kode*

> Hvordan fungerer kommunikasjonen for dere?

*veldig bra*

> Gjør et kort retrospektiv hvor dere vurderer hva dere har klart til nå, og hva som kan forbedres. Dette skal handle om prosjektstruktur, ikke kode. Dere kan selvsagt diskutere kode, men dette handler ikke om feilretting, men om hvordan man jobber og kommuniserer.

*Prosjektet ser bra ut, men vi ser på fremtiden at det kan bli en del arbeid for å få prosjektet til å virkelig stå på egne bein*

> Under vurdering vil det vektlegges at alle bidrar til kodebasen. Hvis det er stor forskjell i hvem som committer, må dere legge ved en kort forklaring for hvorfor det er sånn. Husk å committe alt. (Også designfiler)

*Vi har jobber sammen og sitter ofte og par programerer slik at det er mye samarbeid som ikke syns, men vi er fornøyd med det alle har bidrat til kodebasen så langt*

>Referat fra møter siden forrige leveranse skal legges ved (mange av punktene over er typisk ting som havner i referat).
> Bli enige om maks tre forbedringspunkter fra retrospektivet, som skal følges opp under neste sprint.

*Vi har lagt ved logg for hvert møte i Discord, dette for at alle skal ha rask og enkelt tilgang til de både på telefon og på pc, vi lager en mappe ved slutten av prosjektet som inneholder alle loggene med dato, noe vi ble enige om som team*