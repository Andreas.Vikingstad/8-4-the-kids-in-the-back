# Rapport – innlevering 1
**Team:** *The Kids in the Back* – *Erik Andreas Nicolai Elisabeth og Snorre*

A1:
Skrevet info i README.md 

A2:
* Spillfigur som kan styres – gå til høyre/venstre, hoppe oppover
* Todimensjonal verden:
   * Plattform – horisontal flate spilleren kan stå eller gå på (inkludert «bakken»)
   * Vegg – vertikal flate som spilleren ikke kan gå gjennom
   * Spilleren beveger seg oppover ved å hoppe, og nedover ved å falle
* Fiender som beveger seg og er skadelige ved berøring
* Spilleren kan samle poeng ved å plukke opp ting
* Utfordringen i spillet er gjerne en eller flere av: å bevege seg gjennom terrenget uten å dø av monstre.
* Samle energidrikk for å bevege seg raskere.
* Verden er større en skjermen og beveger seg horisentalt/vertikalt i forhold til spillerens posisjon
* verden er bygget opp av blokker med fats størrelse på 20x20.

A3:
Prosjektmetodikken vi har brukt er mye parprogrammering, ettersom det har hjulpet oss å kunne diskutere kode og lage den best mulig. Det har hjulpet å få forskjellige perspektiver mens man programmer og gjør at man skriver den mer effektivt.

Vi skal lage ett Platformer spill som kalles dungeon run. Målet for applikasjonen er å ha 3-4 baner du må gjennom for å kunne vinne, vi vil ha 2 enemies minst og 1-2 objekter du kan interacte med altså plukke opp. Vi vil ha mulighet til å velge mellom noen forskjellige karakter design. Du skal bruke taster for å kunne bevege spiller. Vi brukte den anbefalte forslaget til en MVP. 

Brukeren av applikasjonen vår vil være generelt folk som liker spill som Super Mario eller lignende platformer spill. Så som en spiller ville jeg ha ønsket at spille skulle runnet smooth og ha minst mulig bugs, slik at det er mulig å spille spillet uten mye avbrytelser. 

Som spiller skal jeg kunne se enemies for å kunne unngå dem, jeg skal også kunne plukke opp en health boost for å kunne få liv dersom jeg mister ett. 

A4: 
Vi startet ved en veldig simpel brikke som kunne bevege seg frem og tilbake, vi valgte Java.AWT da vi er vandt til det, vi har par programert for å teste ut rammeverket. 

A5: 
Vi prøvde oss på en liten og veldig kort platformer, vi satt oss ned å kodet det sammen, litt rot i starten men fant fort flyetn som vi tat med oss inn videre i prosjektet. Vi fikk noe problemer med hoppe mekanismen da vi hadde uenigheter om hvordan dette skulle utføres som førte til kluss i prosjektet. Vi tar med oss videre å jobbe litt i Branches å bruke merge request toolen for å kunne ha bedre kontroll over hvor forskjellene i koden vår ligger. Dette gir også bedre oversikt for å kunne jobbe i eget tempo på forskjellige branches uten for mye kluss, vi har lært.. 

